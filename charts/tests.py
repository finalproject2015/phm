from django.test import TestCase
import views
import random
import os
from models import UserPlugins
from django.contrib.auth.models import User
from phm.domain.DataExpSystem import DataExpSystem

#python manage.py test charts.tests


class chartsViewsTestCase(TestCase):


    def setUp(self):
        self.u1 = User.objects.create(username='user1')
        self.admin = User.objects.create(username='admin')
        os.environ['PHM_BASE_DIR'] = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))


    """
    test_one_graph_query- test single graph build_query with a client.get request. it contains 50 different tests with random values for graph's properties
    """
    def test_one_graph_query(self):
        for x in range(0,50):
            #random values
            head=random.randint(1,12)
            led=random.randint(1,11)
            type=random.randint(1,4)
            if (led==9):
                lad="all"
            elif (led==10):
                led="long"
            elif (led==11):
                led="short"
            if (type==1):
                type="temperature"
            elif (type==2):
                type="voltage"
            elif (type==3):
                type="current"
            else:
                type="power"
            response = self.client.get('/home/',{u'formset-0-Datum Type': [u''+str(type)+''],u'table_plugin': [u''],u'start_date': [u'19/06/2014 12:41'],u'end_date': [u'19/06/2015 12:41'], u'formset-0-Head': [u''+str(head)+''], u'formset-0-Plugins': [u'degradationTable'], u'request': [u''], u'formset-TOTAL_FORMS': [u'1'], u'formset-MAX_NUM_FORMS': [u'1000'], u'formset-MIN_NUM_FORMS': [u'0'], u'formset-0-Panel': [u'1'], u'formset-INITIAL_FORMS': [u'0'], u'csrfmiddlewaretoken': [u'mgS5K8GIoQchJdXRKcdqobd97UMdm4hH'], u'formset-0-Led': [u''+str(led)+''], u'formset-0-Machine': [u'1']})
            r=response.wsgi_request
            chart_data = views.build_request(r,'test')
            ans="settings between 19/06/2014 12:41 and 19/06/2015 12:41 table plugin  panel "+ str(type)+"  graph  Datum Type "+ str(type)+"  Machine 1  Head "+str(head) +"  Led "+str(led)+"  plugins degradationTable "
            print ('test #'+str(x+50))
            #####################################
            ##############test i#################
            self.assertEqual(chart_data,ans)
            #####################################
            print ('    pass')


    """
    test_invalid_input_values test single graph query with no keywords
    """
    def test_invalid_input_no_keywords(self):
        response = self.client.get('/home/',{u'request': [u'no keywords string']})
        r=response.wsgi_request
        error_occured = False
        chart_data = views.build_request(r,'test')
        table_plugins = {}
        chart_plugins = {}
        plugins_values = {'table_plugins': table_plugins, 'chart_plugins': chart_plugins}
        dataExpSystem = DataExpSystem()
        try:
            dataExpSystem.get_data_from_query(chart_data,plugins_values)
        except Exception as e:
            error_occured = True
        self.assertTrue(error_occured)


    """
    test_invalid_input_out_of_bound_machine test single graph query with machine value out of bound
    """
    def test_invalid_input_out_of_bound_machine(self):
        #build dict for request
        response = self.client.get('/home/',{u'request': [u'panel graph machine 10 head 1 led 2 temperature']})
        r=response.wsgi_request
        error_occured = False
        chart_data = views.build_request(r,'test')
        table_plugins = {}
        chart_plugins = {}
        plugins_values = {'table_plugins': table_plugins, 'chart_plugins': chart_plugins}
        dataExpSystem = DataExpSystem()
        obj=dataExpSystem.get_data_from_query(chart_data,plugins_values)
        self.assertEqual(obj['chart_panels']['panels'][0]['graphs'][0]['graph_data'],[])


    """
    test_invalid_input_out_of_bound_minus test single graph query with random param get minus out of bound value
    """
    def test_invalid_input_out_of_bound_minus(self):
        #build dict for request
        param=random.randint(1,3)
        machine=1
        head=1
        led=1
        if param==1:
            machine=-1
        elif param==2:
            head=-1
        else:
            led=-1
        response = self.client.get('/home/',{u'request': [u'panel graph machine '+str(machine)+' head '+str(head)+' led '+str(led)+' temperature']})
        r=response.wsgi_request
        error_occured = False
        chart_data = views.build_request(r,'test')
        table_plugins = {}
        chart_plugins = {}
        plugins_values = {'table_plugins': table_plugins, 'chart_plugins': chart_plugins}
        dataExpSystem = DataExpSystem()
        try:
            dataExpSystem.get_data_from_query(chart_data,plugins_values)
        except Exception as e:
            error_occured = True
        self.assertTrue(error_occured)


    """
    test_invalid_input_out_of_bound_Head test single graph query with head value out of bound    """
    def test_invalid_input_out_of_bound_Head(self):
        #build dict for request
        response = self.client.get('/home/',{u'request': [u'panel graph machine 1 head 100 led 2 temperature']})
        r=response.wsgi_request
        error_occured = False
        chart_data = views.build_request(r,'test')
        table_plugins = {}
        chart_plugins = {}
        plugins_values = {'table_plugins': table_plugins, 'chart_plugins': chart_plugins}
        dataExpSystem = DataExpSystem()
        obj=dataExpSystem.get_data_from_query(chart_data,plugins_values)
        self.assertEqual(obj['chart_panels']['panels'][0]['graphs'][0]['graph_data'],[])


    """
    test_invalid_input_out_of_bound_Led test single graph query with led value out of bound
    """
    def test_invalid_input_out_of_bound_Led(self):
        #build dict for request
        response = self.client.get('/home/',{u'request': [u'panel graph machine 1 head 1 led 200 temperature']})
        r=response.wsgi_request
        error_occured = False
        chart_data = views.build_request(r,'test')
        table_plugins = {}
        chart_plugins = {}
        plugins_values = {'table_plugins': table_plugins, 'chart_plugins': chart_plugins}
        dataExpSystem = DataExpSystem()
        obj=dataExpSystem.get_data_from_query(chart_data,plugins_values)
        self.assertEqual(obj['chart_panels']['panels'][0]['graphs'][0]['graph_data'][0],None)


    """
    test_invalid_input_unknown_datum test single graph query with unknown datum type
    """
    def test_invalid_input_unknown_datum(self):
        #build dict for request
        response = self.client.get('/home/',{u'request': [u'panel graph machine 1 head 1 led 2 someUnknownDatum']})
        r=response.wsgi_request
        error_occured = False
        chart_data = views.build_request(r,'test')
        table_plugins = {}
        chart_plugins = {}
        plugins_values = {'table_plugins': table_plugins, 'chart_plugins': chart_plugins}
        dataExpSystem = DataExpSystem()
        try:
            obj=dataExpSystem.get_data_from_query(chart_data,plugins_values)
        except Exception as e:
            self.assertEqual(e.message,"There is not any datum")
            error_occured = True
        self.assertTrue(error_occured)


    """
    test_multiple_panels test single graph multiple panels query with a client.get request. it contains 50 different tests with random values for graphs properties
    """
    def test_multiple_panels(self):
        for test in range(0,50):
            s = ''
            counter=0
            for x in range(1,2):
                panels=random.randint(2,4)
                a={u'start_date': [u'20/06/2014 12:40'],u'end_date': [u'20/06/2015 12:40'],u'csrfmiddlewaretoken': [u'UhRH7ut4qaRLQ5ev88R8SV7HV6B1czEz'], u'formset-TOTAL_FORMS': [u''+str((panels-1)*3)+''], u'request': [u''],u'favourites': [u''],u'table_plugin': [u''] ,u'formset-MIN_NUM_FORMS': [u'0'],u'formset-INITIAL_FORMS': [u'0'],u'submit': [u'Submit '], u'fav_name': [u''],u'formset-MAX_NUM_FORMS': [u'1000']}
                s+= "settings between 20/06/2014 12:40 and 20/06/2015 12:40 table plugin  "
                for y in range(1,panels):
                    type=random.randint(1,4)
                    if (type==1):
                        type="temperature"
                    elif (type==2):
                        type="voltage"
                    elif (type==3):
                        type="current"
                    else:
                        type="power"
                    s +=  "panel "+str(type)+' '
                    a['mynewkey'] = 'mynewvalue'
                    for z in range(0,3):
                        head=random.randint(1,12)
                        led=random.randint(1,11)
                        if (led==9):
                            lad="all"
                        elif (led==10):
                            led="long"
                        elif (led==11):
                            led="short"
                        a[u'formset-'+str(counter)+'-Datum Type']= u''+str(type)+''
                        a[u'formset-'+str(counter)+'-Plugins']= u'removeZeroes'
                        a[ u'formset-'+str(counter)+'-Machine']= u'1'
                        a [u'formset-'+str(counter)+'-Head']=u''+str(head)+''
                        a [u'formset-'+str(counter)+'-Panel']= u''+str(y)+''
                        a[ u'formset-'+str(counter)+'-Led']=u''+str(led)+''
                        s +=  " graph  Datum Type "+ str(type)+"  Machine 1  Head "+str(head) +"  Led "+str(led)+"  plugins removeZeroes "
                        counter=counter+1
            response = self.client.get('/home/', a)
            r=response.wsgi_request
            chart_data = views.build_request(r,'test')
            print ('test #'+str(test))
            #####################################
            ##############test i#################
            self.assertEqual(s,chart_data)
            #####################################
            print ('    pass')


    """
    test_favorites test illegal access to Favorites DB
    """
    def test_favorites(self):
        s1="settings between 19/06/2014 12:41 and 19/06/2015 12:41 table plugin  panel temperature  graph  Datum Type temperature  Machine 1  Head 5  Led 3  plugins degradationTable"
        s2="settings between 19/06/2014 12:41 and 19/06/2015 12:41 table plugin  panel voltage  graph  Datum Type voltage  Machine 1  Head 6  Led 2  plugins degradationTable"
        created=views.save_favorite(s1, self.u1, "test1-success")
        self.assertEqual(created,"Query has been saved successfully")
        created=views.save_favorite(s2, self.u1, "test2-same name")
        self.assertEqual(created,"Query has been saved successfully")
        created=views.save_favorite(s2, self.u1, "test2-same name")
        self.assertEqual(created,"You already have a query with the same name, please choose a unique name ")
        error_occured = False
        try:
            created=views.save_favorite(s1, self.u1, "test3-same query")
        except:
            error_occured = True
        self.assertTrue(error_occured)


    """
    test_initial_plugins in admin and user profile
    """
    def test_initial_plugins(self):
        pluginsList= {'table_plugins': {'plugin1': {'A': 3}},
                      'chart_plugins':
                          {'plugin2': {'Y': 10, 'X': 1},
                           'plugin3': {'B': 10, 'C':1}}}
        tmp=[]
        Utmp=[]
        views.plugins_db('test',pluginsList)
        Adminuser = User.objects.get(username='admin')
        admin_plugins = UserPlugins.objects.all().filter(user=Adminuser.id)
        for p in admin_plugins:
            tmp.append(p.plugin_name)

        views.initial_plugins(self.u1,'test' ,pluginsList )
        user_plugins = UserPlugins.objects.all().filter(user=self.u1.id)
        for p in user_plugins:
            Utmp.append(p.plugin_name)
        #check admin
        self.assertEqual(len(admin_plugins),5)
        self.assertTrue('plugin1' in tmp )
        self.assertTrue('plugin2' in tmp )
        self.assertTrue('plugin3' in tmp )
        self.assertFalse('plugin4' in tmp )
        #check user
        self.assertEqual(len(user_plugins),5)
        self.assertTrue('plugin1' in Utmp )
        self.assertTrue('plugin2' in Utmp )
        self.assertTrue('plugin3' in Utmp )
        self.assertFalse('plugin4' in Utmp )


    """
    test_new_plugins test the ability to add new plugins in admin and user profile
    """
    def test_new_plugins(self):
        pluginsList= {'table_plugins': {'plugin1': {'A': 3}},
                      'chart_plugins':
                          {'plugin2': {'Y': 10, 'X': 1},
                           'plugin3': {'B': 10, 'C':1},
                           'plugin4': {'D': 10}
                          }}
        pluginsList1= {'table_plugins': {'plugin1': {'A': 3}},
                      'chart_plugins':
                          {'plugin2': {'Y': 10, 'X': 1},
                           'plugin3': {'B': 10, 'C':1},
                           'plugin4': {'D': 10},
                           'plugin5': {'E': 11}
                          }}
        tmp=[]
        Utmp=[]
        views.initial_plugins(self.u1,'test' ,pluginsList )
        Adminuser = User.objects.get(username='admin')
        admin_plugins = UserPlugins.objects.all().filter(user=Adminuser.id)
        for p in admin_plugins:
            tmp.append(p.plugin_name)
        user_plugins = UserPlugins.objects.all().filter(user=self.u1.id)
        for p in user_plugins:
            Utmp.append(p.plugin_name)
        #check admin
        self.assertEqual(len(admin_plugins),6)
        self.assertTrue('plugin1' in tmp )
        self.assertTrue('plugin2' in tmp )
        self.assertTrue('plugin3' in tmp )
        self.assertTrue('plugin4' in tmp )
        #check user
        self.assertEqual(len(user_plugins),6)
        self.assertTrue('plugin1' in Utmp )
        self.assertTrue('plugin2' in Utmp )
        self.assertTrue('plugin3' in Utmp )
        self.assertTrue('plugin4' in Utmp )
        views.initial_plugins(self.u1,'test' ,pluginsList1 )
        admin_plugins = UserPlugins.objects.all().filter(user=Adminuser.id)
        for p in admin_plugins:
            tmp.append(p.plugin_name)
        user_plugins = UserPlugins.objects.all().filter(user=self.u1.id)
        for p in user_plugins:
            Utmp.append(p.plugin_name)
        #check admin
        self.assertEqual(len(admin_plugins),7)
        self.assertTrue('plugin5' in tmp )
        #check user
        self.assertEqual(len(user_plugins),7)
        self.assertTrue('plugin5' in Utmp )


    """
    test_change_value_plugins test the ability to change values of plugins
    """
    def test_change_value_plugins(self):
        pluginsList= {'table_plugins': {'plugin1': {'A': 3}},
                      'chart_plugins':
                          {'plugin2': {'Y': 10, 'X': 1},
                           'plugin3': {'B': 10, 'C':1},
                           'plugin4': {'D': 10},
                           'plugin5': {'E': 11}
                          }}
        views.initial_plugins(self.u1,'test' ,pluginsList )
        views.change_plugins_value(self.u1 ,'chart_plugins+plugin4+D', 23 )
        Adminuser = User.objects.get(username='admin')
        admin_plugins = UserPlugins.objects.get(user=Adminuser.id , plugin_name='plugin4', param='D' )
        Avalue=str(admin_plugins.value)
        user_plugins = UserPlugins.objects.get(user=self.u1.id, plugin_name='plugin4', param='D' )
        Uvalue=str(user_plugins.value)
        self.assertEqual(Avalue,'10' )
        self.assertEqual(Uvalue , '23' )



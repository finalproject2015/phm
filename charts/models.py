
import dateutil.relativedelta
from phm.domain.ConfigurationFileReader import ConfigurationFileReader
from django.contrib.auth.models import User
from datetime import datetime
from django.db import models
from django import forms


class UserPermission(models.Model):
    """
    Model that describes the permissions of the users.
    Each user can have several permissions.

    """
    user = models.ForeignKey(User, unique=False)
    permission = models.CharField(max_length=30,unique=False)

    def __str__(self):
        return self.user.username

    class Meta:
        ordering = ('user',)


class Favorites(models.Model):
    """
    Model that defines favorites in the system. Each Favorite Model Configured with 3 parameters:
        Query is The query string that the user want to save as favorite.
        User is the user name of the  query owner.
        Query name is the name of the query for displaying to the user.
    """
    query = models.CharField(max_length=2000, unique=True)
    user = models.ForeignKey(User, unique=False)
    query_name = models.CharField(max_length=100, unique=False)

    def __str__(self):
        return self.query

    class Meta:
        ordering = ('query',)


class UserPlugins(models.Model):
    """
    Model that defines user plugins. Each user holds unique plug-ins parameters values.
    Each UserPlugins Model Configured with the following parameters:
        User is the plug-in parameter value user.
        Plugin-type is the type of the plugin, can be either chart or table.
        Plug-in name is the name of the plug-in.
        Param is parameter name.
        Value is the parameter value that the user defines.

    """
    user = models.ForeignKey(User, unique=False)
    plugin_type = models.CharField(max_length=100, unique=False)
    plugin_name = models.CharField(max_length=100, unique=False)
    param = models.CharField(max_length=100, unique=False)
    value = models.CharField(max_length=100, unique=False)

    def __str__(self):
        return '%s: %s' % (self.user.username, self.plugin_name)

    class Meta:
        ordering = ('user',)


class GraphForm(forms.Form):
    """
    Model that defines Graph form. This Graph has several fields that builds dynamically according to
    configuration file Entities.  Each Graph Defines mandatory fields and dynamic fields:
        Panel is The panel number that will displayed on the chart.  [default maximum panels: 10]
        Datum Type is the datum type of the panel\graph.
        Entities according to configuration file.
        Plugins is the chart plug-ins of the graph.

    """

    def __init__(self, *args, **kwargs):
        super(GraphForm, self).__init__(*args, **kwargs)

        self.fields['Panel'] = forms.ChoiceField(choices=[(x, x) for x in range(1, 10)])
        self.fields['Datum Type'] = forms.ChoiceField(choices=[(x, x) for x in ConfigurationFileReader().getMeasureDatums().keys()])
        for headerKey, headersValues in ConfigurationFileReader().getHeaders().items():
            if headerKey == 'Plugins':
                self.fields[headerKey] = forms.ChoiceField(choices=[(x, x) for x in ['']+headersValues])
            else:
                self.fields[headerKey] = forms.ChoiceField(choices=[(x, x) for x in headersValues])


class Settings(forms.Form):
    """
    Model that defines settings form. This Form Configured with 3 fields:
        No chart is parameter that allow us to show\hide the chart data.
        No table is parameter that allow us to show\hide the table data.
        Table Plugins is the table plug-ins of the chart.

    """
    no_chart = forms.BooleanField()
    no_table = forms.BooleanField()
    try:
        table_plugin = forms.ChoiceField(choices=[(x, x) for x in ['']+ConfigurationFileReader().getTablePlugins()])
    except Exception as e:
        pass


class TimeRange(forms.Form):
    """
    Model that defines time range  form. This Form Configured with 2 fields:
        Start Date is start date of the data to process and visualize..
        End Date is end date of the data to process and visualize.

    """
    start_date = forms.DateTimeField(initial=(datetime.now()-dateutil.relativedelta.relativedelta(years=1)).strftime('%d/%m/%Y %H:%M'),
                                     widget=forms.TextInput(attrs={'class': 'datepicker'}))
    end_date = forms.DateTimeField(initial=datetime.now().strftime('%d/%m/%Y %H:%M'),
                                   widget=forms.TextInput(attrs={'class': 'datepicker'}))
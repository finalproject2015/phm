
import json
import logging
from phm.domain.ConfigurationFileReader import ConfigurationFileReader
from phm.domain.DataExpSystem import DataExpSystem
from django.shortcuts import render_to_response
from django.core.context_processors import csrf
from phm.domain.PhmException import PhmException
from django.http import HttpResponseRedirect
from django.contrib.auth.models import User
from django.template import RequestContext
from models import UserPermission
from django.contrib import auth
from models import UserPlugins
from forms import GraphFormset
from models import Favorites
from models import Settings
from models import TimeRange
from itertools import groupby


def login(request):
    return render_to_response('login.html', csrf(request))


def auth_view(request):
    username = request.POST.get('username', '')
    password = request.POST.get('password', '')
    user = auth.authenticate(username=username, password=password)

    if user is not None and has_permission(user):

        auth.login(request, user)
        return HttpResponseRedirect('/home')
    else:
        if user is not None and not has_permission(user):
            error = "You don't have permission"
        else:
            error = "username or password incorrect"
        return render_to_response('login.html', {'error': error}, context_instance=RequestContext(request))


def logout(request):
    auth.logout(request)
    return HttpResponseRedirect('/login')


def has_permission(user):
    user_permissions = [user_p.permission for user_p in UserPermission.objects.all().filter(user=user)]
    return ConfigurationFileReader().getPermissionLevel() in user_permissions


def favorites(request):
    if request.user.id is None:
        return render_to_response("login.html", RequestContext(request, {}))

    if 'submit' not in request.GET:
        fav_table = [(e.query_name, e.id) for e in Favorites.objects.all().filter(user=request.user)]
        return render_to_response('favorites.html', {'fav_table': fav_table, 'username': request.user}, context_instance=RequestContext(request))
    else:
        values_to_remove = request.GET.getlist('checked')
        loop = len(values_to_remove)
        for x in range(0, loop):
            Favorites.objects.filter(id=values_to_remove[x]).delete()
        fav_table = [(e.query_name, e.id) for e in Favorites.objects.all().filter(user=request.user)]
        return render_to_response('favorites.html', {'fav_table': fav_table, 'username': request.user}, context_instance=RequestContext(request))


def plugins(request):
    if request.user.id is None:
        return render_to_response("login.html", RequestContext(request, {}))

    if 'save' in request.GET:
        for param in request.GET:
            if not param == 'save':
                change_plugins_value(request.user, param,request.GET[param])

    chart_plugins_info = UserPlugins.objects.all().filter(user=request.user.id, plugin_type='chart_plugins')
    chart_plugins_info = {key: list(group) for key, group in groupby(chart_plugins_info, lambda x: x.plugin_name)}

    table_plugins_info = UserPlugins.objects.all().filter(user=request.user.id, plugin_type='table_plugins')
    table_plugins_info = {key: list(group) for key, group in groupby(table_plugins_info, lambda x: x.plugin_name)}

    return render_to_response('Plugins.html',
                              {'chart_plugins': chart_plugins_info, 'table_plugins': table_plugins_info, 'username': request.user},
                              context_instance=RequestContext(request))


def change_plugins_value(user, param , value):
    tmp = param.split('+')
    user_plugins = UserPlugins.objects.get(user=user, plugin_type=tmp[0], plugin_name=tmp[1], param=tmp[2])
    user_plugins.value = value  # field changed
    user_plugins.save()


def home(request):
    if request.user.id is None:
        return render_to_response("login.html", RequestContext(request, {}))
    if not has_permission(request.user):
        error = "You don't have permission"
        return render_to_response('login.html', {'error': error}, context_instance=RequestContext(request))

    formset = GraphFormset(request.GET or None, prefix='formset')
    settings = Settings(request.GET or None)
    time_range = TimeRange(request.GET or None)
    fav_list = [' ']+[e.query_name for e in Favorites.objects.all().filter(user=request.user.id)]

    initial_plugins(request.user,'real',[])

    return render_to_response('home.html',
                              {'settings': settings,
                               'time_range': time_range,
                               'formset': formset,
                               'Fav_table': fav_list,
                               'username': request.user},
                              context_instance=RequestContext(request))


def initial_plugins(user , flag, list):
    # plug-ins
    UpluginsName=[]
    ApluginsName=[]
    plugins_db(flag,list)

    # user plug-ins
    plugins_table = UserPlugins.objects.all().filter(user=user.id)
    for Up in plugins_table:
        UpluginsName.append(Up.plugin_name)

    #adin plug-ins
    Adminuser = User.objects.get(username='admin')
    admin_plugins = UserPlugins.objects.all().filter(user=Adminuser.id)
    for Ap in admin_plugins:
        ApluginsName.append(Ap.plugin_name)

    #new plug-ins in db
    if len(plugins_table) < len(admin_plugins):
        for Ap in set(ApluginsName):
            if not Ap in set(UpluginsName):
                newplugins= UserPlugins.objects.all().filter(user=Adminuser.id ,plugin_name=Ap  )

                for p in newplugins:
                    UserPlugins(user=user, plugin_type=p.plugin_type, plugin_name=p.plugin_name, param=p.param, value=p.value).save()


def annotationChart(request):
    table_plugins = UserPlugins.objects.all().filter(user=request.user.id, plugin_type='table_plugins')
    chart_plugins = UserPlugins.objects.all().filter(user=request.user.id, plugin_type='chart_plugins')
    plugins_values = {'table_plugins': table_plugins, 'chart_plugins': chart_plugins}

    if request.GET['favourites'] == '':
        if request.method == 'GET' and 'submit' in request.GET:
            try:
                dataExpSystem = DataExpSystem()
                chart_data = dataExpSystem.get_data_from_query(build_request(request,'real'), plugins_values)

            except PhmException as e:
                logging.error('Got Exception: unit -> %s , message -> %s' % (e.unit, e.message))
                exception = {'title': e.unit, 'message': e.message, 'bottom': 'Please Contact The PHM Supervisor'}
                return render_to_response('annotationChart.html', {'chart_data': json.dumps(getDefaultChart()), 'exception': json.dumps(exception)})
            except Exception as e:
                logging.error('Got Exception: unit -> General exception , message -> %s' % e.message)
                exception = {'title': 'General Exception', 'message': e.message, 'bottom': 'Please Contact The PHM Supervisor'}
                return render_to_response('annotationChart.html', {'chart_data': json.dumps(getDefaultChart()), 'exception': json.dumps(exception)})
            logging.info('Rendered to response to page \'annotationChart.html\'')
            return render_to_response('annotationChart.html', {'chart_data': json.dumps(chart_data), 'exception': json.dumps({'empty': True})})
        elif not request.GET['fav_name'] == '':
            try:
                q_name = request.GET['fav_name']
                dataExpSystem = DataExpSystem()
                data = build_request(request, 'real')
                chart_data = dataExpSystem.get_data_from_query(data, plugins_values)
                owner = request.user
                created=save_favorite(data,owner,q_name)
                return render_to_response('annotationChart.html', {'created':created,'chart_data': json.dumps(chart_data), 'exception': json.dumps({'empty': True})})
            except:
                obj=Favorites.objects.all().filter(user=owner,query=data)
                s="This query is already in your favorites list: "+str(obj[0].query_name)
                return render_to_response('annotationChart.html', {'created':s,'chart_data': json.dumps(chart_data), 'exception': json.dumps({'empty': True})})
    else:
        fav_name = request.GET['favourites']
        tmp = Favorites.objects.get(query_name=fav_name)
        try:
            dataExpSystem = DataExpSystem()
            chart_data = dataExpSystem.get_data_from_query(str(tmp), plugins_values)
        except PhmException as e:
            logging.error('Got Exception: unit -> %s , message -> %s' % (e.unit, e.message))
            exception = {'title': e.unit, 'message': e.message, 'bottom': 'Please Contact The PHM Supervisor'}
            return render_to_response('annotationChart.html', {'chart_data': json.dumps(getDefaultChart()), 'exception': json.dumps(exception)})
        except Exception as e:
            logging.error('Got Exception: unit -> General exception , message -> %s' % e.message)
            exception = {'title': 'General Exception', 'message': e.message, 'bottom': 'Please Contact The PHM Supervisor'}
            return render_to_response('annotationChart.html', {'chart_data': json.dumps(getDefaultChart()), 'exception': json.dumps(exception)})
        logging.info('Rendered to response to page \'annotationChart.html\'')
        return render_to_response('annotationChart.html', {'chart_data': json.dumps(chart_data), 'exception': json.dumps({'empty': True})})


def save_favorite(data,owner,q_name):
    fav = Favorites.objects.all().filter(user=owner,query_name=q_name)
    if not fav:
        Favorites.objects.get_or_create(query=data, user=owner, query_name=q_name)
        return "Query has been saved successfully"
    else:
        return "You already have a query with the same name, please choose a unique name "


def getDefaultChart():
    return {'chart_panels': {'date': [],
                             'table': {'columns': [{}], 'dataSet': []},
                             'panels': [{'panel_datum': '', 'events': [], 'graphs': []}]}}


def build_request(r, t):
    if r.GET["request"] == '':
        myDict = r.GET.dict()
        fields_arr = ['Datum Type', 'Machine', 'Head', 'Led', 'Plugins']  # For Testing
        fields_arr = ConfigurationFileReader().getHeaders().keys() if t == 'real' else fields_arr
        # Creating dictionary of { panel_num: [graphs] }
        panels = {}
        for p_loop in range(0, int(myDict['formset-TOTAL_FORMS'][0])):
            panel_value = int(myDict["formset-%d-Panel" % p_loop])
            panels[panel_value] = panels[panel_value]+[p_loop] if panel_value in panels else [p_loop]

        ans = 'settings '
        ans += '-nochart ' if 'no_chart' in myDict and myDict['no_chart'] == 'on' else ''
        ans += '-notable ' if 'no_table' in myDict and myDict['no_table'] == 'on' else ''
        ans += 'between %s and %s ' % (myDict['start_date'], myDict['end_date'])
        ans += 'table plugin %s ' % myDict['table_plugin']
        for panel_num, panel_graphs in sorted(panels.items()):

            if len(set([myDict['formset-%d-Datum Type' % p] for p in panel_graphs])) != 1:
                raise PhmException('Illegal Query', 'Panel %d has more than 1 Datum!' % panel_num)

            ans += 'panel %s ' % myDict['formset-%d-Datum Type' % panel_graphs[0]]
            for graph_id in panel_graphs:
                ans += ' graph '
                for entity in fields_arr:
                    field = 'formset-%d-%s' % (graph_id, entity)
                    if entity.lower() == 'plugins':
                        ans += ' plugins '
                        plugins_list = [myDict[field]]  # converting to list for future causes
                        for p in range(0, len(plugins_list)):
                            ans += '%s ' % plugins_list[p]
                    else:
                        ans += ' %s %s ' % (entity, myDict[field])
        return ans
    else:
        clean_line = r.GET["request"].replace('+', ' ')
        return clean_line


def plugins_db(flag,plugins_test_list):
    if flag=='real':
        user = User.objects.get(username='admin')
        plugins_list = ConfigurationFileReader().getPluginsParameters()

        for plugin_type, plugins in plugins_list.items():
            for plugin_name, plugins_params in plugins.items():
                for param, value in plugins_params.items():
                    UserPlugins.objects.get_or_create(user=user,
                                                      plugin_type=plugin_type,
                                                      plugin_name=plugin_name,
                                                      param=param,
                                                      value=value)
    else:
        user = User.objects.get(username='admin')
        plugins_list = plugins_test_list
        for plugin_type, plugins in plugins_list.items():
            for plugin_name, plugins_params in plugins.items():
                for param, value in plugins_params.items():
                    UserPlugins.objects.get_or_create(user=user,
                                                      plugin_type=plugin_type,
                                                      plugin_name=plugin_name,
                                                      param=param,
                                                      value=value)


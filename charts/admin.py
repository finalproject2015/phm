from django.contrib import admin
from models import UserPlugins
from models import Favorites
from models import UserPermission


admin.site.register(UserPlugins)
admin.site.register(Favorites)
admin.site.register(UserPermission)
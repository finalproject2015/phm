var chart;

function createMultipleGraphsChart(chart_data) {
	/*********************************************************
     * Initialize Properties From Django                     *
     *********************************************************/
	var chart_dates = chart_data["chart_panels"]["date"];
	var chart_panels = chart_data["chart_panels"]["panels"];
	var balloonText = "[[]]"//"[[category]]<br/><b>value: [[value]]</b>";

	var chartData = [];
	for (d in chart_dates) {
		var sample = {"date": new Date(chart_dates[d])};
		for (p in chart_panels) {
			var panel_graphs = chart_panels[p]["graphs"];
			for (g in panel_graphs) {
				if (panel_graphs[g]["graph_data"][d] == null)
					sample[panel_graphs[g]["graph_id"]] = undefined;
				else
					sample[panel_graphs[g]["graph_id"]] = panel_graphs[g]["graph_data"][d];
			}
		}
		chartData.push(sample);
	}

	/*********************************************************
	 * Creating The Requested Chart                          *
     *********************************************************/
	chart = new AmCharts.AmStockChart();
	chart.pathToImages = "/static/amcharts/images/";
	chart.amExport = {imageFileName: "phmImage", 
				enabled: true,
				libs: { "path": "/static/amcharts/plugins/export/libs/"},
				periodSelector: {"position": "left"},
				dataSetSelector: {"position": "left"}};
	chart.precision = 2;
	
	var colors = ['#0D8ECF', '#DA0CDD', '#CD0D74', '#CC0000', '#00CC00', 
				  '#0000CC', '#DDDDDD', '#999999', '#333333', '#990000'];

	//Data Set
	var dataSet = new AmCharts.DataSet();
	dataSet.dataProvider = chartData;
	dataSet.categoryField = "date";
	
	var fieldMappings = [];
	for (p in chart_panels) {
		var panel_graphs = chart_panels[p]['graphs'];
		for (g in panel_graphs) {
			fieldMappings.push({
				fromField: panel_graphs[g]["graph_id"],
				toField: panel_graphs[g]["graph_id"]
			});
		}
	}
	dataSet.fieldMappings = fieldMappings;
	chart.dataSets = [dataSet];
	
	var color_index = 0;
	var panels = []
	for (var p = 0; p < chart_panels.length ; p++) {
		var panel_graphs = chart_panels[p]['graphs'];
		var panel_events = chart_panels[p]['events'];
		
		// Creating the panels
		var stockPanel = new AmCharts.StockPanel();
		stockPanel.recalculateToPercents = "never";
		stockPanel.startDuration = 0.4;
		
		if (p != chart_panels.length-1)
			stockPanel.showCategoryAxis = false;
		if (p != 0 && p != chart_panels.length-1)
			stockPanel.allowTurningOff = true;
			
		if (chartData.length == 0)
			stockPanel.allLabels = [{"x":0, "y":100, "text":"No data available in Chart", "align":"center", "size":20}];
		
		// Stock legend
		var stockLegend = new AmCharts.StockLegend();
		stockLegend.valueTextRegular = undefined;
		stockLegend.markerType = "diamond";
		stockPanel.categoryAxis.dashLength = 0;
		stockPanel.valueTextRegular = "[[value]]";
		stockLegend.valueTextRegular = "[[value]]";
		stockLegend.periodValueTextRegular = "[[value.open]]";
		stockLegend.valueTextComparing = "[[value]] ([[percents.value]])%";
		stockLegend.periodValueTextComparing = "[[value.open]] ([[percents.value.open]])%";
		stockLegend.spacing = 30;
		stockPanel.stockLegend = stockLegend;
		
		//Value Axis
		var valueAxis = new AmCharts.ValueAxis();
		valueAxis.dashLength = 0;
		valueAxis.title = chart_panels[p]["panel_datum"];
		valueAxis.labelOffset = 7;
		stockPanel.addValueAxis(valueAxis);
		
		//Chart Cursor
		var chartCursor = new AmCharts.ChartCursor();
		chartCursor.valueLineEnabled = true;
		chartCursor.valueLineAxis = valueAxis;
		chartCursor.bulletsEnabled = true;
		chartCursor.graphBulletSize = 1;
		stockPanel.chartCursor = chartCursor;
		
		var graph;
		for (var g = 0; g < panel_graphs.length ; g++) {
			// Creating the graphs of the current panel
			graph = new AmCharts.StockGraph();
			graph.title = panel_graphs[g]["graph_name"];
			graph.valueField = panel_graphs[g]["graph_id"];
			graph.periodValue = "Average";
			graph.useDataSetColors = false;
			graph.lineColor = colors[color_index%colors.length];
			graph.showBalloon = true;
			graph.compareFromStart = true;
			graph.type = "smoothedLine";
			graph.cornerRadiusTop = 5;
			graph.bullet = "none";
			graph.bulletBorderColor = colors[color_index%colors.length];
			graph.bulletBorderAlpha = 1;
			graph.bulletBorderThickness = 1;
			graph.bulletColor = "#FFFFFF";
			graph.hideBulletsCount = 0;
			graph.useDataSetColors = false;
			graph.balloonText = balloonText;
			graph.fillAlphas = 0.02;
			//graph.connect = false;
			
			if (p==0 && g==0) {
				// Scroll Bar Settings
				var scrollbarSettings = new AmCharts.ChartScrollbarSettings();
				scrollbarSettings.graph = graph;
				scrollbarSettings.updateOnReleaseOnly = false;
				scrollbarSettings.usePeriod = "ss"; // this will improve performance
				scrollbarSettings.position = "bottom";
				chart.chartScrollbarSettings = scrollbarSettings;
			}
			color_index++;
			stockPanel.addStockGraph(graph);
		}
		// Dealing with Events
		for (var e = 0; e < panel_events.length ; e++) {
			var event = panel_events[e];
			var stockEvent = new AmCharts.StockEvent();
			stockEvent.date = new Date(event[0]);
			stockEvent.graph = graph;
			stockEvent.text = "E";
			stockEvent.description = '<font color="red"><b>'+event[1]+'</font></b><br>'+event[2];
			stockEvent.backgroundAlpha = 1;
			stockEvent.borderAlpha = 1;
			stockEvent.showOnAxis = true;
			dataSet.stockEvents.push(stockEvent);	
		}
		
		// Putting guide in current date
		var guide = new AmCharts.Guide();
		var curr_date = new Date();
		guide.date = curr_date;
		guide.inside = true;
		guide.above = true;
		guide.label = "Current Date";
		guide.balloonText = curr_date;
		guide.lineColor = "#CC0000";
		guide.lineAlpha = 0.2;
		guide.labelRotation = 90;
		guide.lineThickness = 3;
		stockPanel.categoryAxis.addGuide(guide);
			
		panels.push(stockPanel);
	}
	
	chart.panels = panels;

	// Stock Event Settings
	var stockEventSettings = new AmCharts.StockEventsSettings();
	stockEventSettings.backgroundColor = "#85CDE6";
	stockEventSettings.balloonColor = "#CC0000";
	stockEventSettings.rollOverColor = "#FFCCAA";
	stockEventSettings.type = "sign";
	chart.stockEventsSettings = stockEventSettings;
	
	// Panels Settings
	var panelsSettings = new AmCharts.PanelsSettings();
	//panelsSettings.mouseWheelZoomEnabled = true;
	panelsSettings.usePrefixes = true;
	panelsSettings.marginRight = 16;
	panelsSettings.marginLeft = 16;
	panelsSettings.creditsPosition = "bottom-right"
	panelsSettings.precision = 4;
	chart.panelsSettings = panelsSettings;

	// Category Axes Settings
	var categoryAxesSettings = new AmCharts.CategoryAxesSettings();
	categoryAxesSettings.minPeriod = "ss";
	categoryAxesSettings.maxSeries = Number.POSITIVE_INFINITY;
	categoryAxesSettings.position = "bottom";
	chart.categoryAxesSettings = categoryAxesSettings;
	
	//Value Axes Settings
	var valueAxesSettings = new AmCharts.ValueAxesSettings();
	valueAxesSettings.precision = 2;
	chart.valueAxesSettings = valueAxesSettings;
	
	// Chart Cursor Settings
	var cursorSettings = new AmCharts.ChartCursorSettings();
	cursorSettings.valueBalloonsEnabled = true;
	cursorSettings.valueLineEnabled = true,
	cursorSettings.valueLineBalloonEnabled = true,
	cursorSettings.cursorAlpha = 0.5;
	chart.chartCursorSettings = cursorSettings;

	// Period Selector
	var periodSelector = new AmCharts.PeriodSelector();
	periodSelector.position = "bottom";
	periodSelector.dateFormat = "DD-MM-YYYY";
	periodSelector.inputFieldWidth = 100;
	periodSelector.periods = [
	   {period:"DD", count:1, label:"1 day"}, 
	   {period:"DD", count:7, label:"1 week"},
	   {period:"MM", count:1, label:"1 month"},
	   {period:"MM", count:6, label:"6 months"},
	   {period:"YYYY", count:1, label:"1 year"},
	   {period:"YTD", label:"YTD"},
	   {period:"MAX", label:"MAX", selected:true}
	];
	chart.periodSelector = periodSelector;
	
	//adding last point (second) for grouping events
	if (chart.dataSets[0].dataProvider.length > 0) {
		var endDate = chart.dataSets[0].dataProvider[chart.dataSets[0].dataProvider.length-1].date;
		chart.dataSets[0].dataProvider.push({
			date: new Date(endDate.getTime() + 1000)
		});
	}
	
	return chart;
}

function clickStockEvent_handler(event) {
	var stockEvent = event["eventObject"];
	var text = stockEvent.date+'<br><br>'+stockEvent.description;
	$("#eventDialog").html(text);
	$("#eventDialog").dialog("open");
}

function show_events(elem) {
	if (elem.checked == true)
		chart.showStockEvents();
	else
		chart.hideStockEvents();
}

function setComparison(elem) {
	var chart_comparison;
	var stockLegendSpacing;
	
	if (elem.checked == true) {
		chart_comparison = "always";
		stockLegendSpacing = 80;
	} else {
		chart_comparison = "never";
		stockLegendSpacing = 30;
	}
	
	for (var p = 0; p < chart.panels.length ; p++) {
		chart.panels[p].recalculateToPercents = chart_comparison;
		chart.panels[p].stockLegend.spacing = stockLegendSpacing;
	}
	chart.validateData();
}

function setChartDisplay(elem) {
	for (var p = 0; p < chart.panels.length ; p++) {
		for (var g = 0; g < chart.panels[p].graphs.length ; g++) {
			chart.panels[p].graphs[g].type = elem.value;	
		}	
	}
	chart.validateData();
}

function groupingSeries(grouping_series) {

	var endDate = chart.dataSets[0].dataProvider[chart.dataSets[0].dataProvider.length-2].date;

	var time = {};
	time["mm"] = 60*1000;
	time["10mm"] = time["mm"]*10;
	time["30mm"] = time["10mm"]*3;
	time["hh"] = time["30mm"]*2;
	time["DD"] = time["hh"]*24
	time["WW"] = time["DD"]*7;
	time["MM"] = time["DD"]*(endDate.getDaysLeftInTheMonth()+1);

	var daysDiff = Math.round(Math.abs((chart.startDate - endDate.getTime())/(time["DD"])));
	var timeData = {};
	timeData["mm"] = daysDiff*24*60;
	timeData["10mm"] = daysDiff*24*6;
	timeData["30mm"] = daysDiff*24*2;
	timeData["hh"] = daysDiff*24;
	timeData["DD"] = daysDiff;
	timeData["WW"] = daysDiff/7;
	timeData["MM"] = daysDiff/(7*12);
	
	if (grouping_series.value == "all") {
		chart.chartScrollbarSettings.usePeriod = "ss";
		chart.categoryAxesSettings.maxSeries = Number.POSITIVE_INFINITY;
		chart.categoryAxesSettings.groupToPeriods = ["ss", "10ss", "30ss", "mm", "hh", "DD", "WW", "MM", "YYYY"];
		
		chart.dataSets[0].dataProvider.pop();
		chart.dataSets[0].dataProvider.push({
            date: new Date(endDate.getTime() + 1000)
        });	
	}
	else {
		chart.chartScrollbarSettings.usePeriod = grouping_series.value;
		chart.categoryAxesSettings.maxSeries = timeData[grouping_series.value];
		chart.categoryAxesSettings.groupToPeriods = [grouping_series.value];
		
		chart.dataSets[0].dataProvider.pop();
		chart.dataSets[0].dataProvider.push({
            date: new Date(endDate.getTime() + time[grouping_series.value])
        });	
	}
	chart.validateData();
}

Date.prototype.getDaysLeftInTheMonth = function () {
    var then = new Date (this.getTime())
    then.setMonth (then.getMonth() + 1)
    then.setDate (0)
    return then.getDate() > this.getDate() ? then.getDate() - this.getDate() : 0
}

function setGroupValue(elem) {
	for (var p = 0; p < chart.panels.length ; p++) {
		for (var g = 0; g < chart.panels[p].graphs.length ; g++) {
			chart.panels[p].graphs[g].periodValue = elem.value;	
		}	
	}
	chart.validateData();
}

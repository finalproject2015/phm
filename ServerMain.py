# -*- coding: utf-8 -*-

import os
import sys
import logging
import traceback
from phm.domain.LogHandler import TimedSizeRotatingFileHandler
from serverUI.ServerAdapter import ServerAdapter
from PySide.QtGui import QApplication
from PySide.QtGui import QMessageBox
from PySide.QtCore import Qt


def main():
    """
    Main Program of the Server
    Initialize the Server and uploading the UI

    """
    app = QApplication(sys.argv)

    try:
        logging.basicConfig(level=logging.DEBUG)
        logger = logging.getLogger('')
        for handler in logger.handlers:
            logger.removeHandler(handler)

        LOG_FILE_SIZE = 1024*1024*2
        LOG_BACKUP_COUNT = 10
        file_path = os.path.join(os.environ.get('PHM_BASE_DIR'), 'config', 'logs')
        rotatingFileHandler = TimedSizeRotatingFileHandler(file_path, 'data_analysis.log', maxBytes=LOG_FILE_SIZE, backupCount=LOG_BACKUP_COUNT)
        formatter = logging.Formatter('%(asctime)s %(levelname)s %(message)s')
        rotatingFileHandler.setFormatter(formatter)
        rotatingFileHandler.setLevel(logging.DEBUG)
        logging.getLogger('').addHandler(rotatingFileHandler)

        errorFileHandler = TimedSizeRotatingFileHandler(file_path, 'data_analysis_err.log', maxBytes=LOG_FILE_SIZE, backupCount=LOG_BACKUP_COUNT)
        errorFileHandler.setFormatter(formatter)
        errorFileHandler.setLevel(logging.ERROR)
        logging.getLogger('').addHandler(errorFileHandler)
        logging.info('Data Exploratory System is Ready!')

        server = ServerAdapter(app)
        server.onShowServer()

    except Exception:
        QMessageBox(QMessageBox.Critical, 'ERROR',
                    'Server: Got Exception\nCheck for problems!\n\n' + traceback.format_exc(),
                    QMessageBox.Ok, None, Qt.WindowStaysOnTopHint).exec_()
        sys.exit(1)

    app.setQuitOnLastWindowClosed(False)
    sys.exit(app.exec_())


if __name__ == "__main__":
    os.environ["PHM_BASE_DIR"] = os.path.dirname(os.path.abspath(__file__))
    main()

from django.conf.urls import patterns, include, url
from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('charts.views',

    url(r'^admin/', include(admin.site.urls)),
    url(r'^home', 'home'),
    url(r'^annotationChart/', 'annotationChart'),
    url(r'^favorites/', 'favorites'),
    url(r'^plugins/', 'plugins'),
    url(r'^login/$',  'login'),
    url(r'^auth/$',  'auth_view'),
    url(r'^logout/$', 'logout'),
   
)
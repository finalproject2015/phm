"""
Created on 15 July 2015
@author: Yaniv Peretz
"""

import time
from utils import *
from logging.handlers import RotatingFileHandler, TimedRotatingFileHandler


class TimedSizeRotatingFileHandler(RotatingFileHandler, TimedRotatingFileHandler):

    def __init__(self, filePath, fileName, maxBytes=0, backupCount=0, mode='a', encoding=None, delay=0, when='MIDNIGHT', interval=1, utc=False):
        """
        Handler for logging to a file, rotating the log file at certain timed intervals.

        If backupCount is > 0, when rollover is done, no more than backupCount files are kept - the oldest ones are deleted.

        By default, the file grows indefinitely. You can specify particular values of maxBytes and backupCount to allow
        the file to rollover at a predetermined size.

        Rollover occurs whenever the current log file is nearly maxBytes in length. If backupCount is >= 1,
        the system will successively create new files with the same pathname as the base file, but with extensions
        ".1", ".2" etc. appended to it. If maxBytes is zero, rollover never occurs.

        :param filePath: file path to save the files
        :type filePath: str
        :param fileName: file name to save the logs
        :type fileName: str
        :param mode: the file stream mode, default='a' (append)
        :type mode: str
        :param maxBytes: maximum bytes per file, default=0
        :type maxBytes: int
        :param backupCount: number of files to backup, default=0
        :type backupCount: int
        :param encoding: used to open the file with that encoding, default=None
        :type encoding: str
        :param delay: If delay is true, then file opening is deferred until the first call to emit(). By default, the file grows indefinitely
        :type delay: int
        :param when: Rotating happens based on the product of when and interval, default='MIDNIGHT'
        :type when: str
        :param interval: Rotating happens based on the product of when and interval, default=1
        :type interval: int
        :param utc: the utc argument is true, times in UTC will be used; otherwise local time is used. default=False
        :type utc: bool

        """
        self.filepath = filePath
        self.filename = fileName
        path = verifyPath(os.path.join(self.filepath, time.strftime("%d%b%Y"), self.filename))
        self.rollOverKind = "Size"

        RotatingFileHandler.__init__(self, path, mode, maxBytes, backupCount)
        TimedRotatingFileHandler.__init__(self, path, when, interval, backupCount, encoding, delay, utc)

    def doRollover(self):
        """
        Do a rollover, as described in __init__().

        """
        if self.rollOverKind == "Size":
            RotatingFileHandler.doRollover(self)

        elif self.rollOverKind == "Time":
            if self.stream:
                self.stream.close()
                self.stream = None

            currentTime = int(time.time())
            self.baseFilename = verifyPath(os.path.join(self.filepath, time.strftime("%d%b%y"), self.filename))
            self.stream = open(self.baseFilename, 'w')
            newRolloverAt = self.computeRollover(currentTime)

            while newRolloverAt <= currentTime:
                newRolloverAt = newRolloverAt + self.interval

            self.rolloverAt = newRolloverAt

    def shouldRollover(self, record):
        """
        Determine if rollover should occur.
        Basically, see if the supplied record would cause the file to exceed the size limit we have
        or the when and interval fields arrived to their execution time.

        """
        if self.stream is None:                 # delay was set...
            self.stream = self._open()

        if self.maxBytes > 0:                   # are we rolling over?
            msg = "%s\n" % self.format(record)
            self.stream.seek(0, 2)              #due to non-posix-compliant Windows feature
            if self.stream.tell() + len(msg) >= self.maxBytes:
                self.rollOverKind = "Size"
                return 1

        t = int(time.time())
        if t >= self.rolloverAt:
            self.rollOverKind = "Time"
            return 1

        return 0
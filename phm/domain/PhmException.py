"""
Created on 15 July 2015
@author: Yaniv Peretz
"""


class PhmException(Exception):
    """
    PhmException is user-defined exceptions that derived from Python Exception class.
    This Exception is represents error that occurs in the PHM system and caught in the server end-point
    in the views, and delivered to the user.
    The PhmException provides us the unit where the error occurred and detailed cause message.

    """

    def __init__(self, unit, message):
        """
        Initializes the PHM Exception and assigning the unit and message of the exception.

        :param unit: the unit code
        :param message: the error message

        """
        super(PhmException, self).__init__(message)
        self.unit = unit
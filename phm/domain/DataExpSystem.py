"""
Created on 15 July 2015
@author: Yaniv Peretz
"""

import logging
from phm.datahandler.QueryParserHandler import QueryParserHandler
from LogHandler import TimedSizeRotatingFileHandler
from utils import *


class DataExpSystem(object):
    """
    DatExpSystem is a class that used for mediating between the server end-point to the domain layer where
    the logic's process are performed.

    """
    __metaclass__ = Singleton

    def __init__(self):
        """
        Initializes the data members, configuring the logging, his handlers and settings.

        """
        self.data_handler = QueryParserHandler()

        logging.basicConfig(level=logging.DEBUG)
        logger = logging.getLogger('')
        for handler in logger.handlers:
            logger.removeHandler(handler)

        LOG_FILE_SIZE = 1024*1024*2
        LOG_BACKUP_COUNT = 10
        file_path = os.path.join(os.environ.get('PHM_BASE_DIR'), 'config', 'logs')
        rotatingFileHandler = TimedSizeRotatingFileHandler(file_path, 'data_analysis.log', maxBytes=LOG_FILE_SIZE, backupCount=LOG_BACKUP_COUNT)
        formatter = logging.Formatter('%(asctime)s %(levelname)s %(message)s')
        rotatingFileHandler.setFormatter(formatter)
        rotatingFileHandler.setLevel(logging.DEBUG)
        logging.getLogger('').addHandler(rotatingFileHandler)

        errorFileHandler = TimedSizeRotatingFileHandler(file_path, 'data_analysis_err.log', maxBytes=LOG_FILE_SIZE, backupCount=LOG_BACKUP_COUNT)
        errorFileHandler.setFormatter(formatter)
        errorFileHandler.setLevel(logging.ERROR)
        logging.getLogger('').addHandler(errorFileHandler)
        logging.info('Data Exploratory System is Ready!')

    def get_data_from_query(self, raw_query, plugins_values):
        """
        Gets raw query from the server end-point and returns data according to query.

        :param raw_query: raw query from the user
        :type raw_query: string
        :param plugins_values: user plugins values
        :type plugins_values: dict
        :return: processed chart data
        :rtype: dict
        """
        return {'chart_panels': self.data_handler.parse_query(raw_query, plugins_values)}
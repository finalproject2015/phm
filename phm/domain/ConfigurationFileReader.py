"""
Created on 15 July 2015
@author: Yaniv Peretz
"""

import json
import logging
import operator
import xmltodict
from yapsy.PluginManager import PluginManager
from ConfigParser import SafeConfigParser
from PhmException import PhmException
from collections import OrderedDict
from xml.etree import ElementTree
from utils import *


class ConfigurationFileReader():
    """
    Configuration File Reader is a class that responsible for reading the configuration file that
    the system is currently working with. This class is a Singleton which created only once.

    """
    __metaclass__ = Singleton

    def __init__(self):
        """
        Initializes the class members and. Performs assigning to the plug-ins managers (chart/table),
        reads the XML configuration file and converts it to JSON format.

        """
        self.chart_manager = PluginManager()
        self.chart_manager.setPluginPlaces([os.path.join(os.environ.get('PHM_BASE_DIR'), 'plugins', 'ChartPlugins')])

        self.table_manager = PluginManager()
        self.table_manager.setPluginPlaces([os.path.join(os.environ.get('PHM_BASE_DIR'), 'plugins', 'TablePlugins')])

        default_path = os.path.join(os.environ.get('PHM_BASE_DIR'), 'config', 'dbConfig.xml')
        config_file_path = os.environ.get('PHM_CONFIG_FILE') if 'PHM_CONFIG_FILE' in os.environ else default_path

        if not os.path.exists(config_file_path):
            raise PhmException('Configuration File Reader', 'No Such Config file')
        try:
            root = ElementTree.parse(config_file_path).getroot()
            self.xml_dict = xmltodict.parse(ElementTree.tostring(root))
            self.xml_dict = self.xml_dict['data']
            json.dumps(self.xml_dict)
            self.validateConfigurationReader()
        except ElementTree.ParseError as e:
            logging.error('corrupted xml file, %s' % e.message)
            raise PhmException('Configuration File Reader', 'corrupted xml file')

    def getDbFilePath(self):
        """
        function that return the D.B file path from the configuration file.

        :return: db file path
        :rtype: string
        :raises PhmException: if the file path does not exists
        """
        try:
            db_file_path = self.xml_dict['db_file']['@path']
            if not os.path.isabs(db_file_path):
                return os.path.join(os.environ.get('PHM_BASE_DIR'), db_file_path)
            return db_file_path
        except:
            logging.error('Failed to get D.B File path')
            raise PhmException('Configuration File Reader', 'Failed to get D.B File path')

    def getPermissionLevel(self):
        """
        function that return the permission level of the D.B file from the configuration file.

        :return: permission level
        :rtype: string
        :raises PhmException: if the permission level does not exists
        """
        try:
            return self.xml_dict['permission']['@level']
        except:
            logging.error('Failed to get Permission level')
            raise PhmException('Configuration File Reader', 'Failed to get Permission level')

    def getMeasureTable(self):
        """
        function that return the measure table name from the configuration file.

        :return: measure table elements
        :rtype: OrderedDict
        :raises PhmException: if the measure table does not exists
        """
        try:
            return self.xml_dict['measure_table']
        except:
            raise PhmException('Configuration File Reader', 'Failed to get Measure Table')

    def getMeasureTime(self):
        """
        function that return the time settings (name, column) from the configuration file.

        :return: measure time
        :rtype: OrderedDict
        :raises PhmException: if the measure time does not exists
        """
        try:
            return self.xml_dict['measure_table']['time']
        except:
            raise PhmException('Configuration File Reader', 'Failed to get Measure Time')

    def getMeasureDatums(self):
        """
        function that reads the measures datum types elements (name, column) from the configuration file
        and return it in the exact order it appears.

        :return: measure datum types
        :rtype: OrderedDict
        :raises PhmException: if the measure datum types does not exists
        """
        try:
            return OrderedDict((e['@name'], e['@column']) for e in self.getMeasureTable()['datum_type']['element'])
        except:
            raise PhmException('Configuration File Reader', 'Failed to get Measure Datums types')

    def getMeasureEntities(self):
        """
        function that reads the measures entities (name, column) from the configuration file
        and return it in the exact order it appears.

        :return: measure entities
        :rtype: OrderedDict
        :raises PhmException: if the measure entities does not exists
        """
        try:
            return OrderedDict((e['@name'], e['@column']) for e in self.getMeasureTable()['entity'])
        except:
            raise PhmException('Configuration File Reader', 'Failed to get Measure Entities')

    def getMeasureEntity(self, entityName):
        """
        function that return the requested measure entity settings from the configuration file.

        :param entityName: requested entity name
        :type entityName: string
        :return: measure Entity
        :rtype: entity elements list
        :raises PhmException: if the requested measure Entity does not exists
        """
        try:
            return [x for x in self.getMeasureTable()['entity'] if x['@name'].lower() == entityName.lower()][0]
        except:
            raise PhmException('Configuration File Reader', 'Failed to get Measure Entity: %s' % entityName)

    def getMeasureEntityElements(self, entityName):
        """
        function that return the requested measure entity elements from the configuration file.

        :param entityName: requested entity name
        :type entityName: string
        :return: measure Entity elements
        :rtype: entity elements list
        :raises PhmException: if the requested measure entity elements does not exists
        """
        try:
            entity = self.getMeasureEntity(entityName)
            entity_elements = entity['element'] if 'element' in entity else None
            return [entity_elements] if entity_elements is not None and type(entity_elements) != list else entity_elements
        except:
            raise PhmException('Configuration File Reader', 'Failed to get Measure Entity Elements')

    def getMeasureEntityUniqueKey(self, entity, entityUniqueKey):
        """
        The configuration file elements has the ability to define unique keys in the elements values.
        This function gets the unique key value from the configuration file and return it.

        :param entity: requested entity name
        :type entity: string
        :param entityUniqueKey: requested unique key
        :type entityUniqueKey: string
        :return: measure Entity unique key value
        :rtype: string
        """
        for key in self.getMeasureEntityElements(entity):
            if key['@name'] == entityUniqueKey:
                return key['#text']
        return entityUniqueKey

    def getMeasureTableColumns(self):
        """
        function that return the measure table columns (entities column) from the configuration file.

        :return: measure table columns
        :rtype: list
        """
        measure_table_columns = [e for e in self.getMeasureEntities().keys()]
        measure_table_columns.append(self.getMeasureTime()['@name'])
        measure_table_columns.extend([d for d in self.getMeasureDatums().keys()])
        return measure_table_columns

    def getHeaders(self):
        """
        function that return the headers (entities name, plugins) as it will display to the user from
        the configuration file

        :return: configuration file headers
        :rtype: OrderedDict
        """
        headers_dict = OrderedDict([])
        for entity in self.getMeasureEntities().keys():
            headers_dict[entity] = [e['@name'] for e in self.getMeasureEntityElements(entity)]
        headers_dict['Plugins'] = self.getChartPlugins()
        return headers_dict

    def getChartPlugins(self):
        """
        function that gets the active chart plug-ins names.

        :return: chart plug-ins list
        :rtype: list
        """
        self.chart_manager.collectPlugins()
        return [plugin.name for plugin in self.chart_manager.getAllPlugins()]

    def getTablePlugins(self):
        """
        function that gets the active table plug-ins names.

        :return: table plug-ins list
        :rtype: list
        """
        self.table_manager.collectPlugins()
        return [plugin.name for plugin in self.table_manager.getAllPlugins()]

    def getPluginsParameters(self):
        """
        function that return both chart and table plug-ins parameters values.

        :return: table and chart plug-ins parameters and their values
        :rtype: dict
        """
        self.chart_manager.collectPlugins()
        self.table_manager.collectPlugins()

        plugins_values = {'table_plugins': {}, 'chart_plugins': {}}
        for plugin in sorted(self.chart_manager.getAllPlugins(), key=operator.attrgetter('name')):
            config = SafeConfigParser()
            config.optionxform = str
            config.read(plugin.path + '.yapsy-plugin')
            plugins_values['chart_plugins'][plugin.name] = {}
            for name, value in config.items('Default Values'):
                plugins_values['chart_plugins'][plugin.name][name] = unpack_value(value)

        for plugin in sorted(self.table_manager.getAllPlugins(), key=operator.attrgetter('name')):
            config = SafeConfigParser()
            config.optionxform = str
            config.read(plugin.path + '.yapsy-plugin')
            plugins_values['table_plugins'][plugin.name] = {}
            for name, value in config.items('Default Values'):
                plugins_values['table_plugins'][plugin.name][name] = unpack_value(value)

        return plugins_values

    def hasEvents(self):
        """
        function that returns whether the events are exists in the configuration file,
        True if it exists, False otherwise.

        :return: returns events existence
        :rtype: bool
        """
        return 'events_table' in self.xml_dict

    def getEventsTable(self):
        """
        function that return the events table elements.

        :return: events table
        :rtype: OrderedDict
        :raises PhmException: if the requested events table does not exists
        """
        try:
            return self.xml_dict['events_table']
        except:
            raise PhmException('Configuration File Reader', 'Failed to get Events Table')

    def getEventsTime(self):
        """
        function that return the events time settings (name, column).

        :return: events time settings
        :rtype: OrderedDict
        :raises PhmException: if the requested events time does not exists
        """
        try:
            return self.xml_dict['events_table']['time']
        except:
            raise PhmException('Configuration File Reader', 'Failed to get Events Time')

    def getEventsLabel(self):
        """
        function that return the events Label settings (name, column).

        :return: events label settings
        :rtype: OrderedDict
        :raises PhmException: if the requested events label does not exists
        """
        try:
            return self.xml_dict['events_table']['label']
        except:
            raise PhmException('Configuration File Reader', 'Failed to get Events Label')

    def getEventsDescription(self):
        """
        function that return the events description settings (name, column).

        :return: events description settings
        :rtype: OrderedDict
        :raises PhmException: if the requested events description does not exists
        """
        try:
            return self.xml_dict['events_table']['description']
        except:
            raise PhmException('Configuration File Reader', 'Failed to get Events Description')

    def getEventsElements(self):
        """
        function that return the event elements settings (name, column).

        :return: events elements settings
        :rtype: dict
        :raises PhmException: if the requested events elements does not exists
        """
        try:
            return {e['@name']: e['@column'] for e in self.xml_dict['events_table']['pruning']['entity']}
        except:
            raise PhmException('Configuration File Reader', 'Failed to get Events Pruning Elements')

    def validateConfigurationReader(self):
        """
        function that validates the configuration file on its content and checking whether it written
        in the right format.

        :raises PhmException: if one of the configuration file throws exception
        """
        validate_measure = [lambda: self.getDbFilePath(),
                            lambda: self.getPermissionLevel(),
                            lambda: self.getMeasureTable(),
                            lambda: self.getMeasureTime(),
                            lambda: self.getMeasureDatums(),
                            lambda: self.getMeasureEntities()]
        validate_events = [lambda: self.getEventsTable(),
                           lambda: self.getEventsTime(),
                           lambda: self.getEventsLabel(),
                           lambda: self.getEventsDescription(),
                           lambda: self.getEventsElements()]

        validation = validate_measure+validate_events if self.hasEvents() else validate_measure

        err_msg = ''
        for validation_func in validation:
            try:
                validation_func()
            except PhmException as e:
                err_msg += '%s\n' % e.message

        if len(err_msg):
            logging.error('Configuration File Reader Error:\n%s' %err_msg)
            raise PhmException('Configuration File Reader', err_msg)
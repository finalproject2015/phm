
import os


class Singleton(type):
    _instances = {}

    def __call__(cls, *args, **kwargs):
        if cls not in cls._instances:
            cls._instances[cls] = super(Singleton, cls).__call__(*args, **kwargs)
        return cls._instances[cls]


def str2bool(b):
    if b.lower() in ('no', '0', 'false'):
        return False
    return True


def enum(*args):
    enums = dict(zip(args, range(len(args))))
    return type('Enum', (), enums)


def verifyPath(file_path):
    if not os.path.exists(os.path.dirname((file_path))):
        os.makedirs(os.path.dirname(file_path))
    return file_path


def rangeGenerator(x):
    result = []
    for part in x.split(','):
        if '-' in part:
            a, b = part.split('-')
            result.extend([str(i) for i in range(int(a), int(b) + 1)])
        else:
            result.append(int(part) if is_int(part) else part)
    result = list(set(result))
    result.sort()
    return result


def is_int(num):
    try:
        return isinstance(int(num), (int, long))
    except ValueError:
        return False


def unpack_value(value):
    """
    Returns the value of a requested parameter with the correct value type

    :param param: value
    :type param: string
    :return: value type
    :rtype: could be bool, int, float, string

    """
    if value == 'True':
        return True
    if value == 'False':
        return False
    try:
        return int(value)
    except ValueError:
        try:
            return float(value)
        except ValueError:
            return value
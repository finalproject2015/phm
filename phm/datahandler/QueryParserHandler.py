"""
Created on 15 July 2015
@author: Yaniv Peretz
"""

import uuid
import logging
import itertools
import QueryHandler
import pandas as pd
from phm.domain.ConfigurationFileReader import ConfigurationFileReader
from phm.datahandler.QueryParser import QueryParser
from phm.domain.utils import *
from datetime import datetime

GraphLocation = enum('DATE')


class QueryParserHandler():
    """
    QueryParserHandler  is a class that used as handler for the query parser.

    """

    def __init__(self):
        """
        Initializes the data members.

        """
        self.queryHandler = QueryHandler.QueryHandler()
        self.configReader = ConfigurationFileReader()

    def parse_query(self, string, plugins_values):
        """
        Function that gets raw query and Parse it to Query object, getting the processed data for each graph
        in each panel according to the query object from the QueryHandler.

        Performs dates merge to one serial time series and 'fixing' the graphs data on each panel to match the
        serial time series (if there is time sample but was not any sample in the graph,  None value will be defined).

        Forth more, this function activate the table plug-in on the processed data

        :param string: raw query from the user
        :type string: string
        :param plugins_values: user plugins values
        :type plugins_values: dict
        :return: complete processed data on the chart and table
        :rtype: dict
        """
        ##############################################
        #  Parsing the Query and Getting the data    #
        ##############################################
        logging.info('Parsing The query to a Chart object')
        query_obj = QueryParser().onGetQueryObj(string, plugins_values)
        #print query_obj

        chart = query_obj.chart
        settings = query_obj.settings
        logging.info('Getting The Chart data from D.B')
        tables = []
        panels = []
        for i, panel in enumerate(chart.panels):
            graphs = []
            for graph in panel.graphs:
                events = []
                entities = [(entity.name, entity.value) for entity in graph.entities]
                query_res = self.queryHandler.getProcessedData(settings.time, panel.datum_type, entities, graph.plugins)

                #  Graphs Handler
                if settings.show_chart:
                    graph_data = query_res['graph']
                    graph_name = ' '.join(' '.join(map(str, l)) for l in entities)
                    graphs.append({'graph_id': str(uuid.uuid4()), 'graph_name': graph_name, 'graph_data': graph_data})
                    #  Events Handler
                    events.extend(query_res['events'])
                #  Table Handler
                columns = self.configReader.getMeasureTableColumns()
                if settings.show_table:
                    tables.append(pd.DataFrame(query_res['graph_table'], columns=columns))
            if settings.show_chart or not i:
                panels.append({'panel_datum': panel.datum_type, 'graphs': graphs, 'events': self.__unitEvents__(events)})

        ##############################################
        #       Merge dates and data for Graphs      #
        ##############################################
        logging.info('Merging dates for panels and graphs')
        dates = set()
        for graphs in panels:
            for graph in graphs['graphs']:
                dates = dates.union(set([seq[GraphLocation.DATE] for seq in graph['graph_data']]))
            dates = dates.union(set([seq[GraphLocation.DATE] for seq in graphs['events']]))
        dates = sorted(dates)

        logging.info('Creating new graphs data according to merged dates')
        for graphs in panels:
            for graph in graphs['graphs']:
                new_graph = []
                graph_dict = dict(graph['graph_data'])
                for i, date in enumerate(dates):
                    new_graph.insert(i, graph_dict[date] if date in graph_dict.iterkeys() else None)
                graph['graph_data'] = new_graph

        ##############################################
        #             Building Table data            #
        ##############################################
        df_table = pd.DataFrame(None, columns=columns)
        for table in tables:
            df_table = pd.merge(df_table, table, how='outer')

        time_column = self.configReader.getMeasureTime()['@name']
        df_table[time_column] = df_table[time_column].apply(lambda t: str(datetime.fromtimestamp(t/1000.0)))

        ##############################################
        #               Table Plug-ins                #
        ##############################################
        table = query_obj.table
        if settings.show_table and table.plugin is not None:
            df_table = table.plugin.plugin_obj.plugin_object.run(df_table, **table.plugin.parameters)

        columns = [{"title": col_name, "class": "center"} for col_name in list(df_table.columns.values)]
        table = {'columns': columns, 'dataSet': map(list, df_table.values)}

        return {'date': dates, 'panels': panels, 'table': table}

    def __unitEvents__(self, events):
        """
        Function that gets list of events lists and unit them to one flat list of events.

        :param events: nested events data frames
        :type events: list
        :return: flat list of events
        :rtype: list
        """
        logging.info('Uniting similar events')
        events.sort()
        return list(events for events,_ in itertools.groupby(events))
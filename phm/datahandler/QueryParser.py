"""
Created on 15 July 2015
@author: Yaniv Peretz
"""

import re
import logging
import itertools
from phm.domain.ConfigurationFileReader import ConfigurationFileReader
from phm.domain.PhmException import PhmException
from yapsy.PluginManager import PluginManager
from phm.domain.utils import *
from datetime import datetime
from dateutil import parser

HIDE_CHART_FLAG = '-nochart'
HIDE_TABLE_FLAG = '-notable'


class QueryParser():
    """
    QueryParser class has a unique role, it gets a string that represents the user query request
    and returns query object with all the required settings.

    """

    def __init__(self):
        """
        Initializes the QueryParser object, assigning the configuration file reader class member,
        assigning the chart/table Plugin managers and collecting the plugins.

        """
        self.configReader = ConfigurationFileReader()

        ##############################################
        #      Configure the plugin Managers         #
        ##############################################
        logging.info('Configuring the plugin manager')
        self.chart_manager = PluginManager()
        self.chart_manager.setPluginPlaces([os.path.join(os.environ.get('PHM_BASE_DIR'), 'plugins', 'ChartPlugins')])
        self.chart_manager.collectPlugins()

        self.table_manager = PluginManager()
        self.table_manager.setPluginPlaces([os.path.join(os.environ.get('PHM_BASE_DIR'), 'plugins', 'TablePlugins')])
        self.table_manager.collectPlugins()

    def onGetQueryObj(self, query, plugins_values):
        """
        Function that gets raw query and plugins parameters values from requesting user and
        returns Query object with all the needed settings according to user query.

        :param query: raw query from user
        :type query: string
        :param plugins_values: user plugins values
        :type plugins_values: dict
        :return: query object
        :rtype: Query
        """
        logging.info('Creating The Query object')
        regex_query = [e.strip() for e in re.compile(r'\s*panel:|panel\s*', flags=re.I).split(query)]
        settings = self.__onGetSettings__(regex_query[0])
        table = self.__onGetQueryTable__(regex_query[0], plugins_values)
        chart = self.__onGetQueryChart__(regex_query[1:], plugins_values)
        return QueryParser.Query(settings, table, chart)

    def __onGetSettings__(self, query):
        """
        Function that gets the settings part in the query, extracts the settings object and
        return the Settings object.

        :param query: settings part in the query
        :type query: string
        :return: settings object
        :rtype: Settings
        """
        settings_regex = [e.strip() for e in re.compile(r'\s*settings:|settings\s*', flags=re.I).split(query)]
        settings_regex = settings_regex[1] if len(settings_regex) > 1 else settings_regex[0]
        show_chart = True if re.compile(r'\s*%s\s*' % HIDE_CHART_FLAG, flags=re.I).search(settings_regex) is None else False
        show_table = True if re.compile(r'\s*%s\s*' % HIDE_TABLE_FLAG, flags=re.I).search(settings_regex) is None else False
        return QueryParser.Settings(self.__onGetQueryTime__(query.lower()), show_table, show_chart)

    def __onGetQueryTime__(self, query):
        """
        Function that gets the settings part in the query and extracts the start time and end time objects
        located in the settings object and returns time object.
        If the time declaration do not exists, default values are 01/01/1970 for start time and current
        datetime for end time

        :param query: settings part in the query that may contains the time objects
        :type query: string
        :return: time object
        :rtype: Time
        """
        logging.info('Getting The Chart Time object')
        temp = query.split(' ')
        startStr = 'between' if 'between' in temp else 'from' if 'from' in temp else False
        endStr = 'and' if 'and' in temp else 'to' if 'to' in temp else False
        if startStr and endStr:
            start = parser.parse(re.search('%s(.*)%s' % (startStr, endStr), query).group(1), fuzzy=True, dayfirst=True)
            end = parser.parse(re.search('%s(.*)%s' % (endStr, ''), query).group(1), fuzzy=True, dayfirst=True)
            if (end - start).total_seconds()*1000 <= 0:
                raise PhmException('Query Parser Error', 'Illegal time range')
            return QueryParser.Time(start, end)
        return QueryParser.Time(datetime(1970, 1, 1), datetime.now())

    def __onGetQueryTable__(self, query, plugins_values):
        """
        Function that gets the table part in the query and extracts the table settings object.

        :param query: table settings part in the query
        :type query: string
        :param plugins_values: user plugins values
        :type plugins_values: dict
        :return: table object
        :rtype: Table
        """
        logging.info('Creating The Table object')
        table = QueryParser.Table(self.__onGetQueryTablePlugin__(query, plugins_values))
        logging.info('Table object results:\n%s' % table)
        return table

    def __onGetQueryTablePlugin__(self, query, plugins_values):
        """
        Function that gets the table part in the query and extracts the table plug-in object if it exists
        (if the user chooses any plug-in to activate).

        :param query: table settings part in the query
        :type query: string
        :param plugins_values: user plugins values
        :type plugins_values: dict
        :return: table plug-in
        :rtype: Plugin
        """
        logging.info('Getting The Table Plugin object')
        regex = re.compile(r'\s*table plugins:|table plugin:|tableplugin:|table plugins|table plugin|tableplugin\s*', flags=re.I).split(query)
        plugins_regex = [e.strip() for e in regex]
        plugins_regex = plugins_regex[1] if len(plugins_regex) > 1 else None
        if plugins_regex is not None:
            plugins_regex = plugins_regex.lower().replace('-nochart', '').replace('-notable', '')
            time_start = 'between' if 'between' in plugins_regex else 'from' if 'from' in plugins_regex else None
            new_query = plugins_regex[:plugins_regex.index(time_start)] if time_start else plugins_regex
            plugins = self.__onGetQueryPlugin__(new_query, self.table_manager, plugins_values['table_plugins'])
            return plugins[0] if plugins else None
        return None

    def __onGetQueryPlugin__(self, query, manager, plugins_from_db):
        """
        Function that gets the plugin part in the query, the current working manager to collect to requested
        plug-in (chart or table), the plugins from db of the requesting user and extracts the plug-in object
        on all its parameters and values.

        :param query: table settings part in the query
        :type query: string
        :param manager:  the current plug-in manager
        :type manager: PluginManager
        :param plugins_from_db: user plugins values
        :type plugins_from_db: dict
        :return: plug-in object
        :rtype: Plugin
        """
        logging.info('Getting The Plugins object')
        plugins_dict = manager.getAllPlugins()
        plugins_names = [p.name.lower() for p in plugins_dict]

        plugins = []
        for q_plugin_name in [p.lower() for p in query.split() if '=' not in p]:
            if q_plugin_name in plugins_names:
                plugin_name = plugins_dict[plugins_names.index(q_plugin_name)].name
            else:
                raise PhmException('Query Parser Error', 'No Such Plugin -> %s' % q_plugin_name)

            sub_query_list = query[query.lower().index(q_plugin_name):].split(' ')
            plugin_params = sub_query_list[1:] if len(sub_query_list) > 1 else []
            param_dict = {}
            for p in [p for p in plugins_from_db if p.plugin_name == plugin_name]:
                param_dict[p.param] = p.value

            for param in plugin_params:
                param_values = param.split('=')
                if len(param_values) == 2:
                    temp_list = [p.lower() for p in param_dict.keys()]
                    if param_values[0].lower() in temp_list:
                        param_dict[param_dict.keys()[temp_list.index(param_values[0])]] = unpack_value(param_values[1])
                    else:
                        raise PhmException('Query Parser Error', 'No Such Plugin Param -> %s' % param_values[0])
                else:
                    break
            plugins.append(QueryParser.Plugin(plugin_name, plugins_dict[plugins_names.index(q_plugin_name)], param_dict))
        return plugins

    def __onGetQueryChart__(self, query, plugins_values):
        """
        Function that gets the chart settings part in the query, extracts the chart object and return it.
        Holds the panels list.

        :param query: chart settings part in the query
        :type query: string
        :param plugins_values: user plugins values
        :type plugins_values: dict
        :return: chart object
        :rtype: Chart
        """
        logging.info('Creating The Chart object')
        chart = QueryParser.Chart(self.__onGetQueryPanels__(query, plugins_values))
        logging.info('Chart object results:\n%s' % chart)
        return chart

    def __onGetQueryPanels__(self, panels_query, plugins_values):
        """
        Function that gets the chart settings part in the query and extracts the Panel object list.

        :param panels_query: chart settings part in the query
        :type panels_query: string
        :param plugins_values: user plugins values
        :type plugins_values: dict
        :return: query panels
        :rtype: list
        """
        logging.info('Getting The Chart Panels object')
        panels = []
        for panelQuery in panels_query:
            datums = self.__onGetQueryDatums__(panelQuery)
            graphs = self.__onGetQueryGraphs__(panelQuery, plugins_values)
            panels.extend([QueryParser.Panel(graphs, datum) for datum in datums])
        return panels

    def __onGetQueryDatums__(self, panelQuery):
        """
        Function that gets the chart settings part in the query, extracts the Datum Type object and return it.

        :param panelQuery: chart settings part in the query
        :type panelQuery: string
        :return: datums types
        :rtype: list
        """
        logging.info('Getting The Panels Datum types objects')
        datums_keys = [i for i in self.configReader.getMeasureDatums().keys()]
        datums_keys_lower = [i.lower() for i in datums_keys]
        datums = [datums_keys[datums_keys_lower.index(val.lower())] for val in panelQuery.split(' ') if val.lower() in datums_keys_lower]
        if not datums:
            raise PhmException('Query Parser Error', 'There is not any datum')
        return datums

    def __onGetQueryGraphs__(self, panelQuery, plugins_values):
        """
        Function that gets the chart settings part in the query, extracts the Graph object list and return the list.

        :param panelQuery: chart settings part in the query
        :type panelQuery: string
        :param plugins_values: user plugins values
        :type plugins_values: dict
        :return: query graphs
        :rtype: Graph list
        """
        logging.info('Getting The Panel Graphs objects')
        graphs = []
        regex_query = filter(None, re.compile(r'\s*graph\s*', flags=re.I).split(panelQuery)[1:])  # The first element unimportant
        for graphQuery in regex_query:
            plugins = self.__onGetQueryChartPlugins__(graphQuery, plugins_values)
            entities = self.__onGetQueryEntities__(graphQuery)
            for iterEntities in itertools.product(*entities):
                graphs.append(QueryParser.Graph(iterEntities, plugins))
        return graphs

    def __onGetQueryChartPlugins__(self, graphQuery, plugins_values):
        """
        Function that gets the chart settings part in the query, extracts the chart plug-ins objects if it exists
        (if the user chooses plug-ins to activate) and return it.

        :param graphQuery: chart settings part in the query
        :type graphQuery: string
        :param plugins_values: user plugins values
        :type plugins_values: dict
        :return: chart plug-ins
        :rtype: Plugin list
        """
        logging.info('Getting The Graphs Plugins objects')
        plugins_query = filter(None, re.compile(r'\s*plugins:|plugins\s*', flags=re.I).split(graphQuery)[1:])  # The first element unimportant
        if plugins_query:
            return self.__onGetQueryPlugin__(' '.join(plugins_query), self.chart_manager, plugins_values['chart_plugins'])
        return []

    def __onGetQueryEntities__(self, graphQuery):
        """
        Function that gets the chart settings part in the query, extracts the entities objects list and
        return a list with complete entities.

        :param graphQuery: chart settings part in the query
        :type graphQuery: string
        :return: entities objects
        :rtype: Entity list
        """
        logging.info('Getting The Graph Entities objects')
        entities = []
        for entity in self.configReader.getMeasureEntities().keys():

            entityMatch = re.findall(entity, graphQuery, flags=re.I)
            if not re.findall(entity, graphQuery, flags=re.I):
                raise PhmException('Query Parser Error', '%s entity is missing' % entity)

            restQuery = graphQuery.split(' ')
            for element in entityMatch:
                try:
                    idx = restQuery.index(element)
                    entityInfo = self.configReader.getMeasureEntity(element)
                    entityValue = entityInfo[restQuery[idx+1]] if restQuery[idx+1] in entityInfo else restQuery[idx+1]
                    entityValue = self.configReader.getMeasureEntityUniqueKey(entity, entityValue)
                    entities.append([QueryParser.Entity(entity, value) for value in rangeGenerator(entityValue)])
                except:
                    raise PhmException('Query Parser Error', '%s entity value is invalid' % entity)
        return entities

    class Query():
        def __init__(self, settings, table, chart):
            self.settings = settings
            self.table = table
            self.chart = chart

        def __repr__(self):
            return 'Query:\n%s\n%s\n%s' % (self.settings, self.table, self. chart)

    class Settings():
        def __init__(self, time, show_table, show_chart):
            self.time = time
            self.show_table = show_table
            self.show_chart = show_chart

        def __repr__(self):
            return '   Settings: show table: %s  show table: %s  time range: %s' % (self.show_table, self.show_chart, self.time)

    class Time():
        def __init__(self, start, end):
            self.start = start
            self.end = end

        def __repr__(self):
            return 'Time between %s and %s' % (self.start, self.end)

    class Plugin():
        def __init__(self, name, plugin_obj, params):
            self.name = name
            self.plugin_obj = plugin_obj
            self.parameters = params

        def __repr__(self):
            params_format = ', '.join(['%s:: %s' % (key, value) for (key, value) in self.parameters.items()])
            return 'Plugin: name: %s , parameters: %s' % (self.name, params_format)

    class Table():
        def __init__(self, plugin):
            self.plugin = plugin

        def __repr__(self):
            return '   Table plugin: %s' % self.plugin.name if self.plugin is not None else '   No Table Plugin'

    class Chart():
        def __init__(self, panels):
            self.panels = panels

        def __repr__(self):
            return '   Chart:\n%s' % ''.join(map(str, self.panels))

    class Panel():
        def __init__(self, graphs, datum_type):
            self.graphs = graphs
            self.datum_type = datum_type

        def __repr__(self):
            return '      Panel: (Datum -> %s)\n%s' % (self.datum_type, ''.join(map(str, self.graphs)))

    class Graph():
        def __init__(self, entities, plugins):
            self.entities = entities
            self.plugins = plugins

        def __repr__(self):
            plugins_names = [plugin.name for plugin in self.plugins]
            plugins_str = ', Plugins: %s' % ', '.join(map(str, plugins_names)) if plugins_names else ''
            return '        Graph: Entities: %s %s\n' % (''.join(map(str, self.entities)), plugins_str)

    class Entity():
        def __init__(self, name, value):
            self.name = name
            self.value = value

        def __repr__(self):
            return 'Entity(%s %s) ' % (self.name, self.value)
"""
Created on 15 July 2015
@author: Yaniv Peretz
"""

import time
import logging
import sqlalchemy
from phm.domain.ConfigurationFileReader import ConfigurationFileReader
from phm.domain.PhmException import PhmException
from phm.domain.utils import is_int
from pandas import DataFrame
from dateutil import parser


class QueryHandler():
    """
    QueryHandler is a class that function as mediator between the dataHandler layer to the D.B,
    the Query Handler responsible for getting graph data from the D.B and process it according
    to the query object.

    """

    def __init__(self):
        """
        Initializes data members and reading XML d.b configuration file.

        """
        self.configFileReader = ConfigurationFileReader()
        engine = sqlalchemy.create_engine('sqlite:///%s' % self.configFileReader.getDbFilePath())
        metadata = sqlalchemy.MetaData(engine)

        ##############################################
        #      Reading XML D.B Configuration file    #
        ##############################################
        self.dbTable = sqlalchemy.Table(self.configFileReader.getMeasureTable()['@name'], metadata, autoload=True)
        self.time = self.dbTable.columns[self.configFileReader.getMeasureTime()['@column']]
        self.datum_type = {de[0]: self.dbTable.columns[de[1]] for de in self.configFileReader.getMeasureDatums().items()}
        self.entities = {e[0]: self.dbTable.columns[e[1]] for e in self.configFileReader.getMeasureEntities().items()}

        if self.configFileReader.hasEvents():
            self.eventsTable = sqlalchemy.Table(self.configFileReader.getEventsTable()['@name'], metadata, autoload=True)
            self.eventsTime = self.eventsTable.columns[self.configFileReader.getEventsTime()['@column']]
            self.eventsLabel = self.eventsTable.columns[self.configFileReader.getEventsLabel()['@column']]
            self.eventsDescription = self.eventsTable.columns[self.configFileReader.getEventsDescription()['@column']]
            self.eventsEntities = {e[0]: self.eventsTable.columns[e[1]] for e in self.configFileReader.getEventsElements().items()}

    def getProcessedData(self, chart_time, element, where=[], plugins=[]):
        """
        Function that responsible for mediating between the dataHandler that providing the graph settings
        to the d.b and extracts the desired raw data from the d.b.

        :param chart_time: chart time object.
        :type chart_time: Time object
        :param element: graph datum type.
        :type element: string
        :param where: list of tuples (entity name, entity value).
        :type where: list
        :param plugins: user plugins values.
        :type plugins: dict
        :return: processed data of the graph, events and table.
        :rtype: dict
        :raises PhmException: when error occurs while processing the data
        """
        try:
            ##############################################
            #       Executing Query for one graph        #
            ##############################################
            logging.info('Executing Query for one graph')
            query = sqlalchemy.select([self.time] + self.datum_type.values())
            query = query.where(self.time >= chart_time.start)
            query = query.where(self.time <= chart_time.end)
            for wh in where:
                query = query.where(self.entities[wh[0]] == wh[1])
            res = query.execute()
            ret = [list(row) for row in res]
            for value in ret:
                value[0] = time.mktime(parser.parse(value[0]).timetuple())*1000

            ##############################################
            #        Executing Query for Events          #
            ##############################################
            logging.info('Executing Query for Events')
            events = []
            if self.configFileReader.hasEvents():
                query = sqlalchemy.select([self.eventsTime, self.eventsLabel, self.eventsDescription])
                query = query.where(self.eventsTime >= chart_time.start)
                query = query.where(self.eventsTime <= chart_time.end)
                for wh in where:
                    if wh[0] in self.eventsEntities:
                        query = query.where(self.eventsEntities[wh[0]] == int(wh[1]))

                events = query.execute()
                events = [list(row) for row in events]
                for event in events:
                    event[0] = time.mktime(parser.parse(event[0]).timetuple())*1000

            ##############################################
            #   Creating Table Model from graph request  #
            ##############################################
            col_names = {self.configFileReader.getMeasureTime()['@column']: self.configFileReader.getMeasureTime()['@name']}
            col_names.update({v: k for k, v in dict(self.configFileReader.getMeasureDatums()).items()})
            graph_table = {col[0]: [int(col[1]) if is_int(col[1]) else col[1]]*len(ret) for col in where}
            graph_table.update({col_names[ent]: [value[i] for value in ret] for i, ent in enumerate(res.keys())})
        except:
            raise PhmException('Query Handler Error', 'Failed to get Data From D.B')

        try:
            ##############################################
            # Applying Plugins on graphs data and events #
            ##############################################
            logging.info('Applying Plugins on graphs data and events')
            dates = [date[0] for date in ret]
            #  If there is no any data - create Empty DataFrame (None)
            datum_values = [datum[1:] for datum in ret] if len([datum[1:] for datum in ret]) else None
            df = DataFrame(datum_values, index=dates, columns=self.datum_type.keys())

            event_dates = [date[0] for date in events]
            event_info = [datum[1:] for datum in events] if len(events) > 0 else None
            event_df = DataFrame(event_info, index=event_dates, columns=['label', 'description'])
            event_obj = Events(event_df)
            for plugin in plugins:
                df = plugin.plugin_obj.plugin_object.run(df, event_obj, **plugin.parameters)
            df = df[element]  # pruning to requested column

            event_df = event_obj.events
            ret = [[df.index[i], df.values.tolist()[i]] for i in range(len(df.index))]
            events = [[event_df.index[i]]+event_df.values.tolist()[i] for i in range(len(event_df.index))]
        except:
            raise PhmException('Query Handler Error', 'Failed to run Plugin on the data')

        return {'graph': ret, 'graph_table': graph_table, 'events': events}


class Events():
    """
    Events class Holds the events of the requested samples.

    """

    def __init__(self, events):
        """
        Initializes the class and assigning events data member.
        events DataFrame has 3 Columns: time, label, description.

        :param events: events of the current request
        :type events: pandas.DataFrame
        """
        self.events = events

    def add_event(self, time, label, description):
        """
        Adding Event that will displayed on the chart.

        :param time: event occur time
        :type time: datetime
        :param label: event label
        :type label: string
        :param description: event description
        :type description: string
        """
        if self.events.empty:
            event_dates = [time]
            event_info = [[label, description]]
            self.events = DataFrame(event_info, index=event_dates, columns=['label', 'description'])
        else:
            self.events.loc[time] = None
            self.events.label.loc[time] = label
            self.events.description.loc[time] = description

    def get_events(self):
        """
        getter of the events object.

        :return: events of the current request
        :rtype: DataFrame
        """
        return self.events
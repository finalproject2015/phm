
import unittest

testmodules = [
    'ConfigFileReaderTestCase.ConfigFileReaderTestCase.test_db_file_path',
    'ConfigFileReaderTestCase.ConfigFileReaderTestCase.test_measure_table',
    'ConfigFileReaderTestCase.ConfigFileReaderTestCase.test_measure_time',
    'ConfigFileReaderTestCase.ConfigFileReaderTestCase.test_measure_datums',
    'ConfigFileReaderTestCase.ConfigFileReaderTestCase.test_measure_entities',
    'ConfigFileReaderTestCase.ConfigFileReaderTestCase.test_headers',
    'ConfigFileReaderTestCase.ConfigFileReaderTestCase.test_events_table',
    'ConfigFileReaderTestCase.ConfigFileReaderTestCase.test_events_time',
    'ConfigFileReaderTestCase.ConfigFileReaderTestCase.test_events_label',
    'ConfigFileReaderTestCase.ConfigFileReaderTestCase.test_events_description',
    'ConfigFileReaderTestCase.ConfigFileReaderTestCase.test_events_elements',
    'ConfigFileReaderTestCase.ConfigFileReaderTestCase.test_measure_table_columns',
    'ConfigFileReaderTestCase.ConfigFileReaderTestCase.test_validate_configuration_reader',
    'QueryParserTestCase.QueryParserTestCase.test_single_panel',
    'QueryParserTestCase.QueryParserTestCase.test_multi_panels',
    'QueryParserTestCase.QueryParserTestCase.test_unique_key',
    'QueryParserTestCase.QueryParserTestCase.test_plugins',
    'QueryParserTestCase.QueryParserTestCase.test_time_range',
    'QueryParserTestCase.QueryParserTestCase.test_table'
    ]

suite = unittest.TestSuite()

for t in testmodules:
    try:
        # If the module defines a suite() function, call it to get the suite.
        mod = __import__(t, globals(), locals(), ['suite'])
        suitefn = getattr(mod, 'suite')
        suite.addTest(suitefn())
    except (ImportError, AttributeError):
        # else, just load all the test cases from the module.
        suite.addTest(unittest.defaultTestLoader.loadTestsFromName(t))

unittest.TextTestRunner().run(suite)

import os
import unittest
from phm.domain.PhmException import PhmException
from phm.domain.ConfigurationFileReader import ConfigurationFileReader


class ConfigFileReaderTestCase(unittest.TestCase):

    def setUp(self):
        os.environ['PHM_BASE_DIR'] = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
        self.configReader = ConfigurationFileReader()

    def is_raised_exception(self, func):
        try:
            func()
        except:
            return True
        return False

    def test_db_file_path(self):
        self.assertFalse(self.is_raised_exception(lambda: self.configReader.getDbFilePath()), 'incorrect D.B file path')
        self.assertTrue(self.configReader.getDbFilePath(), 'incorrect D.B file path')

    def test_measure_table(self):
        self.assertFalse(self.is_raised_exception(lambda: self.configReader.getMeasureTable()), 'incorrect measure table')
        self.assertTrue(self.configReader.getMeasureTable()['@name'], 'no measure table name')
        self.assertTrue(self.configReader.getMeasureTable()['time'], 'no measure time')
        self.assertTrue(self.configReader.getMeasureTable()['datum_type'], 'no measure datum type')
        self.assertTrue(self.configReader.getMeasureTable()['entity'], 'no measure entities')
        self.assertTrue(len(self.configReader.getMeasureTable()['entity']) >= 1, 'incorrect entities number')

    def test_measure_time(self):
        self.assertFalse(self.is_raised_exception(lambda: self.configReader.getMeasureTime()), 'incorrect measure time')
        self.assertTrue(self.configReader.getMeasureTime()['@name'], 'no measure time name')
        self.assertTrue(self.configReader.getMeasureTime()['@column'], 'no measure time column')

    def test_measure_datums(self):
        self.assertFalse(self.is_raised_exception(lambda: self.configReader.getMeasureDatums()), 'incorrect measure datums')
        self.assertTrue(len(self.configReader.getMeasureDatums()) >= 1, 'incorrect datums number')

    def test_measure_entities(self):
        self.assertFalse(self.is_raised_exception(lambda: self.configReader.getMeasureEntities()), 'incorrect measure entities')
        self.assertTrue(self.configReader.getMeasureEntities(), 'no measure entities')
        self.assertTrue(len(self.configReader.getMeasureEntities()) >= 1, 'no measure entities')

    def test_headers(self):
        self.assertFalse(self.is_raised_exception(lambda: self.configReader.getHeaders()), 'No headers')
        self.assertTrue('Plugins' in self.configReader.getHeaders(), 'no Plugins header')
        self.assertTrue(len(self.configReader.getHeaders()) > 2, 'no enough headers')

    def test_events_table(self):
        self.assertFalse(self.is_raised_exception(lambda: self.configReader.getEventsTable()), 'No events table')
        self.assertTrue(self.configReader.getEventsTable()['@name'], 'no events name')
        self.assertTrue(self.configReader.getEventsTable()['time'], 'no events time')
        self.assertTrue(self.configReader.getEventsTable()['label'], 'no events label')
        self.assertTrue(self.configReader.getEventsTable()['description'], 'no events description')
        self.assertTrue(self.configReader.getEventsTable()['pruning'], 'no pruning elements')

    def test_events_time(self):
        self.assertFalse(self.is_raised_exception(lambda: self.configReader.getEventsTime()), 'No events time')
        self.assertTrue(self.configReader.getEventsTime(), 'no events time')

    def test_events_label(self):
        self.assertFalse(self.is_raised_exception(lambda: self.configReader.getEventsLabel()), 'No events label')
        self.assertTrue(self.configReader.getEventsLabel(), 'no events label')

    def test_events_description(self):
        self.assertFalse(self.is_raised_exception(lambda: self.configReader.getEventsDescription()), 'No events description')
        self.assertTrue(self.configReader.getEventsDescription(), 'no events description')

    def test_events_elements(self):
        self.assertFalse(self.is_raised_exception(lambda: self.configReader.getEventsElements()), 'No events elements')
        self.assertTrue(self.configReader.getEventsElements(), 'no events pruning')

    def test_measure_table_columns(self):
        self.assertFalse(self.is_raised_exception(lambda: self.configReader.getMeasureTableColumns()), 'No events table columns')
        self.assertTrue(self.configReader.getMeasureTableColumns(), 'no events pruning')
        self.assertTrue(len(self.configReader.getMeasureTableColumns()) >= 2, 'no events pruning')

    def test_validate_configuration_reader(self):
        self.assertFalse(self.is_raised_exception(lambda: self.configReader.validateConfigurationReader()), 'validation failed')
        self.assertRaises(PhmException, self.configReader.validateConfigurationReader(), 'validation failed')
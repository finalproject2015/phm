
import os
import unittest
from phm.domain.PhmException import PhmException
from phm.datahandler.QueryParser import QueryParser


class QueryParserTestCase(unittest.TestCase):

    class UserPlugins():
        def __init__(self, plugin_name, param, value):
            self.plugin_name = plugin_name
            self.param = param
            self.value = value

    def setUp(self):
        os.environ['PHM_BASE_DIR'] = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
        self.queryParser = QueryParser()

        chart_plugins = [QueryParserTestCase.UserPlugins('dataDomain', 'min_value', 22.2),
                         QueryParserTestCase.UserPlugins('dataDomain', 'max_value', 22.6),
                         QueryParserTestCase.UserPlugins('movingAverage', 'window', 5)]
        table_plugins = [QueryParserTestCase.UserPlugins('degradationTable', 'referencePoints', 3)]
        self.plugins_values = {'table_plugins': table_plugins, 'chart_plugins': chart_plugins}

    def is_raised_exception(self, func):
        try:
            func()
        except:
            return True
        return False

    def test_single_panel(self):
        single_panel_query = 'panel Temperature graph machine 1 head 1 led 1,2'
        query_obj = self.queryParser.onGetQueryObj(single_panel_query, self.plugins_values)
        self.assertEqual(len(query_obj.chart.panels), 1, 'incorrect panel number')
        self.assertEqual(len(query_obj.chart.panels[0].graphs), 2, 'incorrect graphs number')

        single_panel_query = 'panel Current graph machine 1 head 1,2 led 3-5'
        query_obj = self.queryParser.onGetQueryObj(single_panel_query, self.plugins_values)
        self.assertEqual(len(query_obj.chart.panels), 1, 'incorrect panel number')
        self.assertEqual(len(query_obj.chart.panels[0].graphs), 6, 'incorrect graphs number')

        single_panel_query = '-nochart panel Current graph machine 1 head 1 led 1'
        query_obj = self.queryParser.onGetQueryObj(single_panel_query, self.plugins_values)
        self.assertFalse(query_obj.settings.show_chart, 'incorrect panel number')

        single_panel_query = 'panel graph machine 1 head 1 led'
        query_obj = self.is_raised_exception(lambda: self.queryParser.onGetQueryObj(single_panel_query, self.plugins_values))
        self.assertTrue(query_obj, 'incorrect query but passed the test')

    def test_multi_panels(self):
        single_panel_query = 'panel Current graph machine 1 head 1 led 1' \
                             'panel Temperature graph machine 2 head 2 led 2' \
                             'panel Power graph machine 3 head 3 led 3'
        query_obj = self.queryParser.onGetQueryObj(single_panel_query, self.plugins_values)
        self.assertEqual(len(query_obj.chart.panels), 3, 'incorrect panel number')
        self.assertEqual(len(query_obj.chart.panels[0].graphs), 1, 'incorrect graphs number')
        self.assertEqual(len(query_obj.chart.panels[1].graphs), 1, 'incorrect graphs number')
        self.assertEqual(len(query_obj.chart.panels[2].graphs), 1, 'incorrect graphs number')

        single_panel_query = 'panel graph machine 1 head 1 led 1,2 temperature current ' \
                             'panel graph machine 2 head 2 led 6-8 power'
        query_obj = self.queryParser.onGetQueryObj(single_panel_query, self.plugins_values)
        self.assertEqual(len(query_obj.chart.panels), 3, 'incorrect panel number')
        self.assertEqual(len(query_obj.chart.panels[0].graphs), 2, 'incorrect graphs number')
        self.assertEqual(len(query_obj.chart.panels[1].graphs), 2, 'incorrect graphs number')
        self.assertEqual(len(query_obj.chart.panels[2].graphs), 3, 'incorrect graphs number')

        single_panel_query = 'panel graph machine 1 head 1 led 1 temperature' \
                             'panel graph unknown data'
        query_obj = self.is_raised_exception(lambda: self.queryParser.onGetQueryObj(single_panel_query, self.plugins_values))
        self.assertTrue(query_obj, 'incorrect query but passed the test')

    def test_unique_key(self):
        single_panel_query = 'panel graph machine 1 head 1 led long temperature'
        query_obj = self.queryParser.onGetQueryObj(single_panel_query, self.plugins_values)
        self.assertEqual(len(query_obj.chart.panels), 1, 'incorrect panel number')
        self.assertEqual(len(query_obj.chart.panels[0].graphs), 2, 'incorrect graphs number')

        single_panel_query = 'panel graph machine 1 head 1,2 led all current'
        query_obj = self.queryParser.onGetQueryObj(single_panel_query, self.plugins_values)
        self.assertEqual(len(query_obj.chart.panels), 1, 'incorrect panel number')
        self.assertEqual(len(query_obj.chart.panels[0].graphs), 16, 'incorrect graphs number')

        single_panel_query = 'panel graph machine 1 head 1 led long voltage' \
                             'panel graph machine 1 head 1 led short voltage' \
                             'panel graph machine 1 head 1 led all voltage'
        query_obj = self.queryParser.onGetQueryObj(single_panel_query, self.plugins_values)
        self.assertEqual(len(query_obj.chart.panels), 3, 'incorrect panel number')
        self.assertEqual(len(query_obj.chart.panels[0].graphs), 2, 'incorrect graphs number')
        self.assertEqual(len(query_obj.chart.panels[1].graphs), 6, 'incorrect graphs number')
        self.assertEqual(len(query_obj.chart.panels[2].graphs), 8, 'incorrect graphs number')

    def test_plugins(self):
        single_panel_query = 'panel graph machine 1 head 1 led short temperature plugins: removeZeroes'
        query_obj = self.queryParser.onGetQueryObj(single_panel_query, self.plugins_values)

        self.assertEqual(len(query_obj.chart.panels[0].graphs[0].plugins), 1, 'incorrect plugins number')
        self.assertEqual(len(query_obj.chart.panels[0].graphs[1].plugins), 1, 'incorrect plugins number')

        single_panel_query = 'panel graph machine 1 head 1 led 1 temperature plugins: removeZeroes movingAverage'
        query_obj = self.queryParser.onGetQueryObj(single_panel_query, self.plugins_values)
        self.assertEqual(len(query_obj.chart.panels[0].graphs[0].plugins), 2, 'incorrect plugins number')

        single_panel_query = 'panel graph machine 1 head 1 led 1 temperature plugins: removeZeroes noSuchPlugin'
        self.assertRaises(PhmException, self.queryParser.onGetQueryObj, single_panel_query, self.plugins_values)

    def test_time_range(self):
        single_panel_query = 'between 15/04/15 13:33 to 20/06/15 14:55 panel graph machine 1 head 1 led long temperature'
        query_obj = self.queryParser.onGetQueryObj(single_panel_query, self.plugins_values)
        self.assertEquals(str(query_obj.settings.time.start), '2015-04-15 13:33:00', 'incorrect start time range')
        self.assertEquals(str(query_obj.settings.time.end), '2015-06-20 14:55:00', 'incorrect end time range')

        single_panel_query = 'between 15/04/15 to 20/06/15 panel graph machine 1 head 1 led long temperature'
        query_obj = self.queryParser.onGetQueryObj(single_panel_query, self.plugins_values)
        self.assertEquals(str(query_obj.settings.time.start), '2015-04-15 00:00:00', 'incorrect start time range')

        single_panel_query = 'between 15/04/15 to 20/01/15 panel graph machine 1 head 1 led long temperature'
        query_obj = lambda: self.queryParser.onGetQueryObj(single_panel_query, self.plugins_values)
        self.assertTrue(self.is_raised_exception(query_obj), 'incorrect time range')

    def test_table(self):
        single_panel_query = 'panel graph machine 1 head 1 led 1,2 temperature'
        query_obj = self.queryParser.onGetQueryObj(single_panel_query, self.plugins_values)
        self.assertTrue(query_obj.settings.show_table, 'incorrect table settings')
        self.assertEqual(query_obj.table.plugin, None, 'incorrect table plugins')

        single_panel_query = 'settings: tableplugin: degradationTable unknown_plugin panel Current graph machine 1 head 1,2 led 3-5'
        self.assertRaises(PhmException, self.queryParser.onGetQueryObj, single_panel_query, self.plugins_values)

        single_panel_query = '-notable panel graph machine 1 head 1,2 led 3-5 current'
        query_obj = self.queryParser.onGetQueryObj(single_panel_query, self.plugins_values)
        self.assertFalse(query_obj.settings.show_table, 'incorrect table settings')
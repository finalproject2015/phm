#!/usr/bin/env python
import os
import sys
from phm.domain.DataExpSystem import DataExpSystem



if __name__ == "__main__":
    os.environ["PHM_BASE_DIR"] = os.path.dirname(os.path.abspath(__file__))
    DataExpSystem()  # Creating DataExpSystem Singleton
    os.environ.setdefault("DJANGO_SETTINGS_MODULE", "phm.settings")
    from django.core.management import execute_from_command_line
    execute_from_command_line(sys.argv)

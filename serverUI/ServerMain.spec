# -*- mode: python -*-

######################################
#    Cleaning the installation env   #
######################################
exec_path = os.path.join(os.getcwd(), 'server_exec')
if os.path.exists(exec_path):
	shutil.rmtree(exec_path)

a = Analysis([os.path.join(os.path.dirname(os.getcwd()), 'ServerMain.py')],
             pathex=[],
             hiddenimports=[],
             hookspath=None,
             runtime_hooks=None)
pyz = PYZ(a.pure)

a.datas += [('serverUI/Extras/styleSheet.qrc', 'Extras/styleSheet.qrc', 'DATA'),
			('serverUI/Extras/icon_phm.png', 'Extras/icon_phm.png', 'DATA'),
			('serverUI/Extras/open_folder.png', 'Extras/open_folder.png', 'DATA')]

exe = EXE(pyz,
          a.scripts,
          a.binaries,
          a.zipfiles,
          a.datas,
          name='server.exe',
          debug=False,
          strip=None,
          upx=True,
          console=True,
		  icon='Extras/icon_phm.ico')

######################################
# Creating The Server Exec Directory #
######################################
os.makedirs(exec_path)
server_source = os.path.join(os.getcwd(), 'dist', 'server.exe')
shutil.copy(server_source, exec_path)

######################################
#      Copying Django Components     #
######################################
phm_base_dir = os.path.dirname(os.getcwd())
shutil.copytree(os.path.join(phm_base_dir, 'charts'), os.path.join(exec_path, 'charts'))
shutil.copytree(os.path.join(phm_base_dir, 'admin'), os.path.join(exec_path, 'admin'))
shutil.copytree(os.path.join(phm_base_dir, 'phm'), os.path.join(exec_path, 'phm'))
shutil.copytree(os.path.join(phm_base_dir, 'plugins'), os.path.join(exec_path, 'plugins'))
shutil.copytree(os.path.join(phm_base_dir, 'config'), os.path.join(exec_path, 'config'))
shutil.copy(os.path.join(phm_base_dir, '__init__.py'), exec_path)
shutil.copy(os.path.join(phm_base_dir, 'manage.py'), exec_path)
shutil.copy(os.path.join(phm_base_dir, 'db.sqlite3'), exec_path)

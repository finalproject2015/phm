"""
Created on 15 July 2015
@author: Yaniv Peretz
"""

import ast
from Generated import PluginParametersDialog
from collections import OrderedDict
from PySide import QtGui


class PluginParametersAdapter(QtGui.QDialog):
    """
    Plugin Parameters Adapter class responsible for getting plug-ins and to enforce the user to set default
    values to the additional parameters he added.

    """

    def __init__(self, parent, plugin_name, plugin_path):
        """
        Initializes the Plugin Parameters Adapter dialog window.

        :param parent: the parent dialog
        :type parent: ServerAdapter
        :param plugin_name: plugin name
        :type plugin_name: string
        :param plugin_path: plug-in file path
        :type plugin_path: string
        """
        super(PluginParametersAdapter, self).__init__(parent)

        self.dialog = PluginParametersDialog.Ui_PluginParametersDialog()
        self.dialog.setupUi(self)
        self.plugin_name = plugin_name
        self.plugin_path = plugin_path
        self.parameters = self.onUpdatePluginParams()
        self.args = None

        self.dialog.lblParam.hide()
        self.dialog.ledtParam.hide()
        self.setConnectivity()

    def setConnectivity(self):
        """
        Function that sets the connectivity between the UI operation to the function it activates.

        """
        self.dialog.btnApply.clicked.connect(self.onApply)
        self.dialog.btnClear.clicked.connect(self.onClear)
        self.dialog.btnCancel.clicked.connect(self.close)

    def onGetArgs(self, source):
        """
        Function that responsible for reading the additional parameters that the user added to the new plug-in.

        :param source: source code file as string
        :type source: string
        :return: getting plug-in arguments
        :rtype: bool or list
        """
        classes = [node for node in ast.walk(ast.parse(source)) if isinstance(node, ast.ClassDef)]
        for cl in classes:
            for b in cl.body:
                if isinstance(b, ast.FunctionDef) and b.name == 'run':
                    args = [arg.id for arg in b.args.args]
                    defaults = [d.n if isinstance(d, ast.Num) else d.s if isinstance(d, ast.Str) else d.id for d in b.args.defaults]
                    return zip(args, ['']*(len(args)-len(defaults))+defaults)[3:]
        return False

    def onUpdatePluginParams(self):
        """
        Function that dynamically creates labels and lineEdits to the plug-in parameters according
        to the given arguments.

        :return: dict with parameter name and his lineEdit
        :rtype: OrderedDict
        """
        self.dialog.lblPluginName.setText('%s - Default values' % self.plugin_name)
        fd = open(self.plugin_path, 'r')
        pluginStr = fd.read()
        args = self.onGetArgs(pluginStr)
        parameters = OrderedDict()

        font = QtGui.QFont()
        font.setPointSize(10)
        for i, arg in enumerate(args, start=1):
            lblParam = QtGui.QLabel(self.dialog.scrollAreaLyt)
            lblParam.setFont(font)
            lblParam.setObjectName(arg[0])
            lblParam.setText('%s: ' % arg[0])
            self.dialog.gridLayout_2.addWidget(lblParam, i, 0, 1, 1)

            ledtParam = QtGui.QLineEdit(self.dialog.scrollAreaLyt)
            ledtParam.setFont(font)
            ledtParam.setObjectName(str(arg[1]))
            ledtParam.setText(str(arg[1]))
            self.dialog.gridLayout_2.addWidget(ledtParam, i, 1, 1, 1)

            parameters[arg[0]] = ledtParam
            if i == len(args):
                spacerItem = QtGui.QSpacerItem(20, 40, QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Expanding)
                self.dialog.gridLayout_2.addItem(spacerItem, i+1, 0, 1, 2)
        return parameters

    def onApply(self):
        """
        Function that checks wether all the parameters assigned with value. if all the parameters was assigned,
        the dialog is closes  and saves the arguments values. If not, warning is displayed to the user.

        """
        for name, ledt in self.parameters.iteritems():
            if ledt.text() == '':
                QtGui.QMessageBox.warning(self, 'Warning', '%s value is Empty!' % name, QtGui.QMessageBox.Ok)
                return
        self.args = OrderedDict([(item[0], item[1].text()) for item in self.parameters.items()])
        self.close()

    def onClear(self):
        """
        Function that clear the parameters values.

        """
        for name, ledt in self.parameters.iteritems():
            ledt.setText('')

    def onShowServer(self):
        """
        Function that shows the plug-in parameters values dialog.

        :return: plug-in parameters values.
        :rtype: OrderedDict
        """
        self.setVisible(True)
        self.raise_()
        self.exec_()
        return self.args
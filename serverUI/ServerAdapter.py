"""
Created on 15 July 2015
@author: Yaniv Peretz
"""

import os
import sys
import time
import signal
import shutil
import socket
import logging
import threading
import subprocess
from PluginParametersAdapter import PluginParametersAdapter
from yapsy.PluginManager import PluginManager
from ConfigParser import ConfigParser
from Generated import ServerDialog
from PySide import QtCore
from PySide import QtGui


class ServerAdapter(QtGui.QDialog):
    """
    Server Adapter class is the Server User Interface which responsible for mediating between the user to the server.
    This class has 2 important abilities, responsible for running the Server with the requested Configuration file path,
    host and ip. And the ability to add new plug-ins to the PHM Server.

    """

    def __init__(self, app):
        """
        Initializes the Server Adapter dialog window, to create the plug-ins managers and to collects the plug-ins.

        :param app: application
        :type app: QApplication
        """
        super(ServerAdapter, self).__init__()
        self.app = app
        self.server_pid = 0

        self.dialog = ServerDialog.Ui_ServerDialog()
        self.dialog.setupUi(self)
        self.setWindowFlags(QtCore.Qt.Window)

        extras_path = self.resource_path('serverUI/Extras')
        try:
            icon = QtGui.QIcon()
            icon.addPixmap(QtGui.QPixmap(os.path.join(extras_path, 'icon_phm.png')), QtGui.QIcon.Normal, QtGui.QIcon.Off)
            self.setWindowIcon(icon)

            with open(os.path.join(extras_path, 'styleSheet.qrc')) as styleSheetFile:
                styleSheet = styleSheetFile.read()
                self.setStyleSheet(styleSheet)
        except:
            pass

        openFolder_pix = QtGui.QPixmap(os.path.join(extras_path, 'open_folder.png'))
        self.dialog.btnBrowseConfigFile.setIcon(openFolder_pix)
        self.dialog.btnBrowseConfigFile.setText('')
        self.dialog.btnTablePluginPathBrowse.setIcon(openFolder_pix)
        self.dialog.btnTablePluginPathBrowse.setText('')
        self.dialog.btnChartPluginPathBrowse.setIcon(openFolder_pix)
        self.dialog.btnChartPluginPathBrowse.setText('')

        self.dialog.tableChartWidgetPlugins.setVerticalScrollMode(QtGui.QAbstractItemView.ScrollPerPixel)
        self.dialog.tableChartWidgetPlugins.setHorizontalScrollMode(QtGui.QAbstractItemView.ScrollPerPixel)
        self.dialog.tableTableWidgetPlugins.setVerticalScrollMode(QtGui.QAbstractItemView.ScrollPerPixel)
        self.dialog.tableTableWidgetPlugins.setHorizontalScrollMode(QtGui.QAbstractItemView.ScrollPerPixel)

        self.dialog.ledtPort.setValidator(QtGui.QIntValidator(0, 65535, self))
        self.dialog.ledtChartVersion.setValidator(QtGui.QDoubleValidator(0, 100, 5, self))
        self.dialog.ledtTableVersion.setValidator(QtGui.QDoubleValidator(0, 100, 5, self))

        self.dialog.btnRunServer.setEnabled(True)
        self.dialog.btnStopServer.setEnabled(False)

        ##############################################
        #      Configure the plugin Managers         #
        ##############################################
        self.chart_plugins_path = os.path.join(os.environ.get('PHM_BASE_DIR'), 'plugins', 'ChartPlugins')
        self.table_plugins_path = os.path.join(os.environ.get('PHM_BASE_DIR'), 'plugins', 'TablePlugins')

        self.chart_manager = PluginManager()
        self.chart_manager.setPluginPlaces([self.chart_plugins_path])
        self.table_manager = PluginManager()
        self.table_manager.setPluginPlaces([self.table_plugins_path])

        self.setConnectivity()
        self.onSetServerLastConnection()
        self.onUpdatePluginsList()
        self.onShowChartPlugins()

    def setConnectivity(self):
        """
        Function that sets the connectivity between the UI operation to the function it activates.

        """
        self.dialog.btnRunServer.clicked.connect(self.onRunServer)
        self.dialog.btnStopServer.clicked.connect(self.onStopServer)
        self.dialog.btnBrowseConfigFile.clicked.connect(self.onBrowseConfigFile)

        self.dialog.cmdChartPluginsEditor.clicked.connect(self.onShowChartPlugins)
        self.dialog.cmdTablePluginsEditor.clicked.connect(self.onShowTablePlugins)

        self.dialog.btnChartPluginPathBrowse.clicked.connect(self.onChartPluginBrowsePath)
        self.dialog.btnChartAddPlugin.clicked.connect(self.onChartAddPlugin)
        self.dialog.btnChartPluginInfoClear.clicked.connect(self.onChartPluginInfoClear)

        self.dialog.btnTablePluginPathBrowse.clicked.connect(self.onTablePluginBrowsePath)
        self.dialog.btnTableAddPlugin.clicked.connect(self.onTableAddPlugin)
        self.dialog.btnTablePluginInfoClear.clicked.connect(self.onTablePluginInfoClear)

    def resource_path(self, relative_path):
        """
        Auxiliary function to gets the files resource path. Since we using PyInstaller we need to deal with
        package in zipped files. PyInstaller store the zipped files in a temp directory,
        Therefore this function gets the real path of the files.

        :param relative_path: the relative path to file
        :type relative_path: string
        :return: absolute real file path
        :rtype: string
        """
        try:
            base_path = sys._MEIPASS
        except Exception:
            base_path = os.path.abspath(".")
        return os.path.join(base_path, relative_path)

    def printLogger(self, message):
        """
        Function that gets a message and display it to the user in a text editor with the current time.

        """
        EOL = '\n' if message[-1] != '\n' else ''
        self.dialog.logger.insertPlainText(time.strftime('%d/%m/%y %H:%M:%S ') + message + EOL)
        self.dialog.logger.ensureCursorVisible()

    def printLoggerInfo(self, message):
        """
        Function that gets information message and display it to the user.
        The information messages are displayed in blue color.

        :param message: message that will displayed in the UI logger
        :type message: string
        """
        self.dialog.logger.setTextColor(QtGui.QColor(0, 0, 255))
        self.printLogger(message)

    def printLoggerError(self, message):
        """
        Function that gets error message and display it to the user.
        The error messages are displayed in red color.

        :param message: message that will displayed in the UI logger
        :type message: string
        """
        self.dialog.logger.setTextColor(QtGui.QColor(255, 0, 0))
        self.printLogger(message)

    def printLoggerCMD(self, message):
        """
        Function that gets messages from the cmd and display it to the user.

        :param message: message that will displayed in the UI logger
        :type message: string
        """
        self.dialog.logger.setTextColor(QtGui.QColor(0, 0, 0))
        self.printLogger(message)

    def onShowServer(self):
        """
        Function that shows the main server dialog and raise it above other programs.

        """
        self.setVisible(True)
        self.raise_()

    def onHideServer(self):
        """
        Function that hides the main server dialog.

        """
        self.setVisible(False)

    def closeEvent(self, event):
        """
        Function that catches the close event and perform independent close after stopping the server.

        :param event: close event
        :type event: QEvent

        """
        logging.info("Server Adapter got closeEvent")
        self.onStopServer()

        self.app.closeAllWindows()
        event.accept()
        self.app.exit()

    def is_valid_ip(self, address):
        """
        Auxiliary function that checks whether the ip address is valid

        :param address: ip address
        :type address: string
        :return: True if address is valid, False otherwise
        :rtype: bool
        """
        try:
            socket.inet_aton(address)
            return True
        except socket.error:
            return False

    def run_cmd(self, cmd):
        """
        Function that execute cmd  command in a new subprocess and prints to screen the cmd messages.

        :param cmd: the desired command
        :type cmd: string
        """
        p = subprocess.Popen(cmd,
                             stdout=subprocess.PIPE,
                             stdin=subprocess.PIPE,
                             stderr=subprocess.STDOUT,
                             shell=True,
                             creationflags=subprocess.CREATE_NEW_PROCESS_GROUP,
                             universal_newlines=True)
        self.server_pid = p.pid

        for line in iter(p.stdout.readline, ''):
            self.printLoggerCMD(line)
            time.sleep(0.01)
        p.stdout.close()
        self.dialog.btnRunServer.setEnabled(True)
        self.dialog.btnStopServer.setEnabled(False)

    ############################################
    #           Server Tab Handler             #
    ############################################
    def onBrowseConfigFile(self):
        """
        Function that responsible for getting the configuration file path from the user.
        This function activate QFileDialog to help the user navigate to requested configuration file.

        """
        config_file_path = QtGui.QFileDialog.getOpenFileName(self, 'Open File', os.getcwd(), "*.xml", '', QtGui.QFileDialog.DontUseNativeDialog)
        config_file_path = config_file_path[0].encode('ascii', 'ignore')
        if config_file_path != '':
            self.dialog.ledtConfigFile.setText(config_file_path)

    def onSetServerLastConnection(self):
        """
        Function that saves the last connection settings in a file to quick server running next server loading.
        The saved settings are configuration file path, host and ip of the server.

        """
        path = os.path.join(os.environ.get('PHM_BASE_DIR'), 'config', 'last_server_connection.dat')
        if os.path.exists(path):
            config = ConfigParser()
            config.optionxform = str
            config.read(path)
            self.dialog.ledtConfigFile.setText(config.get('Server', 'Configuration File'))
            self.dialog.ledtHost.setText(config.get('Server', 'Host'))
            self.dialog.ledtPort.setText(config.get('Server', 'Port'))

    def onRunServer(self):
        """
        Function that runs the server. This function responsible for checking the given information to run
        the server appropriately.

        """
        self.dialog.btnStopServer.setEnabled(True)
        self.dialog.btnRunServer.setEnabled(False)

        config_path = self.dialog.ledtConfigFile.text()
        host = self.dialog.ledtHost.text() if self.dialog.ledtHost.text() != '' else 'localhost'
        port = int(self.dialog.ledtPort.text()) if self.dialog.ledtPort.text() != '' else 8000
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

        error = ''
        if not os.path.exists(config_path):
            error = 'No Such File:\n%s' % config_path
        elif not config_path.lower().endswith('.xml'):
            error = 'Wrong File:\n%s\nUse only .xml Files' % config_path
        elif host != 'localhost' and not self.is_valid_ip(host):
            error = 'Host Ip address -> %s invalid!' % host
        elif not sock.connect_ex((host, port)):
            error = 'Address %s:%d is already Open\n choose another address' % (host, port)

        if error:
            QtGui.QMessageBox.warning(self, 'Warning', error, QtGui.QMessageBox.Ok)
            self.dialog.btnRunServer.setEnabled(True)
            self.dialog.btnStopServer.setEnabled(False)
            return

        self.printLoggerInfo('Starting PHM Server...')
        path = os.path.join(os.environ.get('PHM_BASE_DIR'), 'config', 'last_server_connection.dat')
        with open(path, 'w') as config_file:
            config = ConfigParser()
            config.optionxform = str
            config.add_section('Server')
            config.set('Server', 'Configuration File', config_path)
            config.set('Server', 'Host', host)
            config.set('Server', 'Port', port)
            config.write(config_file)

        os.environ['PHM_CONFIG_FILE'] = config_path
        cmd_thread = threading.Thread(target=self.run_cmd, args=['python manage.py runserver %s:%d' % (host, port)])
        cmd_thread.start()

    def onStopServer(self):
        """
        Function that stops the server appropriately.

        """
        if self.server_pid > 0:
            os.kill(self.server_pid, signal.CTRL_BREAK_EVENT)
            time.sleep(0.1)  # waiting for process to die
            self.server_pid = 0
            self.printLoggerInfo('Server has Stopped..')

    ############################################
    #        Plug-in Manager Tab Handler       #
    ############################################
    def onShowChartPlugins(self):
        """
        Function that moves to chart plug-ins editor screen. Under this screen we can explore the existing
        chart plug-ins and to add new chart plug-ins.

        """
        self.dialog.frameChartPlugins.show()
        self.dialog.frameTablePlugins.hide()

    def onShowTablePlugins(self):
        """
        Function that moves to table plug-ins editor screen. Under this screen we can explore the existing
        table plug-ins and to add new table plug-ins.

        """
        self.dialog.frameTablePlugins.show()
        self.dialog.frameChartPlugins.hide()

    def onUpdatePluginsList(self):
        """
        Function that update both table and chart plug-ins list.

        """
        self.chart_manager.collectPlugins()
        self.table_manager.collectPlugins()

        self.dialog.tableChartWidgetPlugins.setRowCount(0)
        self.dialog.tableTableWidgetPlugins.setRowCount(0)

        for row, plugin in enumerate(self.chart_manager.getAllPlugins()):
            self.dialog.tableChartWidgetPlugins.insertRow(self.dialog.tableChartWidgetPlugins.rowCount())
            self.dialog.tableChartWidgetPlugins.setItem(row, 0, QtGui.QTableWidgetItem(str(plugin.name)))
            self.dialog.tableChartWidgetPlugins.setItem(row, 1, QtGui.QTableWidgetItem(str(plugin.author)))
            self.dialog.tableChartWidgetPlugins.setItem(row, 2, QtGui.QTableWidgetItem(str(plugin.version)))
            self.dialog.tableChartWidgetPlugins.setItem(row, 3, QtGui.QTableWidgetItem(str(plugin.website)))
            self.dialog.tableChartWidgetPlugins.setItem(row, 4, QtGui.QTableWidgetItem(str(plugin.description)))

        for row, plugin in enumerate(self.table_manager.getAllPlugins()):
            self.dialog.tableTableWidgetPlugins.insertRow(self.dialog.tableTableWidgetPlugins.rowCount())
            self.dialog.tableTableWidgetPlugins.setItem(row, 0, QtGui.QTableWidgetItem(str(plugin.name)))
            self.dialog.tableTableWidgetPlugins.setItem(row, 1, QtGui.QTableWidgetItem(str(plugin.author)))
            self.dialog.tableTableWidgetPlugins.setItem(row, 2, QtGui.QTableWidgetItem(str(plugin.version)))
            self.dialog.tableTableWidgetPlugins.setItem(row, 3, QtGui.QTableWidgetItem(str(plugin.website)))
            self.dialog.tableTableWidgetPlugins.setItem(row, 4, QtGui.QTableWidgetItem(str(plugin.description)))

    def onChartPluginBrowsePath(self):
        """
        Function that responsible for getting the new chart plug-in file path from the user.
        This function activate QFileDialog to help the user navigate to requested plug-in file.

        """
        plugin_path = QtGui.QFileDialog.getOpenFileName(self, 'Open File', os.getcwd(), "*.py", '', QtGui.QFileDialog.DontUseNativeDialog)
        plugin_path = plugin_path[0].encode('ascii', 'ignore')
        if plugin_path != '':
            self.dialog.ledtChartPluginPath.setText(plugin_path)

    def onChartAddPlugin(self):
        """
        Function that responsible to add the new desired chart plug-in.
        This function checks whether the new plug-in information is valid (file path is exists,
        plug-in name is entered and validating that this plug-in name is not already exists),
        if there is a problem with the user information appropriate message will be displayed to the user.

        """
        plugin_path = self.dialog.ledtChartPluginPath.text()
        name = self.dialog.ledtChartPluginName.text()
        author = self.dialog.ledtChartAuthor.text()
        version = self.dialog.ledtChartVersion.text()
        website = self.dialog.ledtChartWebsite.text()
        description = self.dialog.ledtChartDescription.text()

        file_name = os.path.splitext(os.path.basename(plugin_path))[0]
        plugin_full_path = os.path.join(self.chart_plugins_path, file_name+'.yapsy-plugin')

        error = ''
        if not os.path.exists(plugin_path):
            error = 'No Such File Path:\n%s' % plugin_path
        elif not plugin_path.lower().endswith('.py'):
            error = 'Wrong File:\n%s\nUse only .py Files' % plugin_path
        elif name == '':
            error = 'Enter Plugin Name!'
        elif os.path.exists(plugin_full_path):
            error = 'This Plug-in already Exist\nPlease Change File name and try again'
        for plugin in self.chart_manager.getAllPlugins():
            if plugin.name == name:
                error = 'This Plug-in Name already In-Use\nPlease Change Plug-in name and try again'
                break

        if error:
            QtGui.QMessageBox.warning(self, 'Warning', error, QtGui.QMessageBox.Ok)
            return

        pluginsDefaultValuesObj = PluginParametersAdapter(self, file_name, plugin_path)
        plugins_default_values = pluginsDefaultValuesObj.onShowServer()
        if plugins_default_values is not None:
            shutil.copy(plugin_path, self.chart_plugins_path)
            ############################################
            #      Creating The yapsy-plugin file      #
            ############################################
            config = ConfigParser()
            config.optionxform = str
            with open(plugin_full_path, 'w') as plugin_file:
                config.add_section('Core')
                config.set('Core', 'Name', name)
                config.set('Core', 'Module', file_name)
                config.add_section('Documentation')
                config.set('Documentation', 'Author', author if author != '' else socket.gethostname())
                config.set('Documentation', 'Version', float(version) if version != '' else 1.0)
                config.set('Documentation', 'Website',  website if website != '' else 'http://www.orbotech.com/')
                config.set('Documentation', 'Description', description if description != '' else name)

                config.add_section('Default Values')
                for default in plugins_default_values.items():
                    config.set('Default Values', default[0], default[1])

                config.write(plugin_file)
            self.onUpdatePluginsList()
            self.onChartPluginInfoClear()

    def onChartPluginInfoClear(self):
        """
        Function that  clears the chart plug-ins information.

        """
        self.dialog.ledtChartPluginPath.setText('')
        self.dialog.ledtChartPluginName.setText('')
        self.dialog.ledtChartAuthor.setText('')
        self.dialog.ledtChartVersion.setText('')
        self.dialog.ledtChartWebsite.setText('')
        self.dialog.ledtChartDescription.setText('')

    def onTablePluginBrowsePath(self):
        """
        Function that responsible for getting the new table plug-in file path from the user.
        This function activate QFileDialog to help the user navigate to requested plug-in file.

        """
        plugin_path = QtGui.QFileDialog.getOpenFileName(self, 'Open File', os.getcwd(), "*.py", '', QtGui.QFileDialog.DontUseNativeDialog)
        plugin_path = plugin_path[0].encode('ascii', 'ignore')
        if plugin_path != '':
            self.dialog.ledtTablePluginPath.setText(plugin_path)

    def onTableAddPlugin(self):
        """
        Function that responsible to add the new desired table plug-in.
        This function checks whether the new plug-in information is valid (file path is exists,
        plug-in name is entered and validating that this plug-in name is not already exists),
        if there is a problem with the user information appropriate message will be displayed to the user.

        """
        plugin_path = self.dialog.ledtTablePluginPath.text()
        name = self.dialog.ledtTablePluginName.text()
        author = self.dialog.ledtTableAuthor.text()
        version = self.dialog.ledtTableVersion.text()
        website = self.dialog.ledtTableWebsite.text()
        description = self.dialog.ledtTableDescription.text()

        file_name = os.path.splitext(os.path.basename(plugin_path))[0]
        plugin_full_path = os.path.join(self.table_plugins_path, file_name+'.yapsy-plugin')

        error = ''
        if not os.path.exists(plugin_path):
            error = 'No Such File Path:\n%s' % plugin_path
        elif not plugin_path.lower().endswith('.py'):
            error = 'Wrong File:\n%s\nUse only .py Files' % plugin_path
        elif name == '':
            error = 'Enter Plugin Name!'
        elif os.path.exists(plugin_full_path):
            error = 'This Plug-in already Exist\nPlease Change File name and try again'
        for plugin in self.table_manager.getAllPlugins():
            if plugin.name == name:
                error = 'This Plug-in Name already In-Use\nPlease Change Plug-in name and try again'
                break

        if error:
            QtGui.QMessageBox.warning(self, 'Warning', error, QtGui.QMessageBox.Ok)
            return

        pluginsDefaultValuesObj = PluginParametersAdapter(self, file_name, plugin_path)
        plugins_default_values = pluginsDefaultValuesObj.onShowServer()
        if plugins_default_values is not None:
            shutil.copy(plugin_path, self.table_plugins_path)
            ############################################
            #      Creating The yapsy-plugin file      #
            ############################################
            config = ConfigParser()
            config.optionxform = str
            with open(plugin_full_path, 'w') as plugin_file:
                config.add_section('Core')
                config.set('Core', 'Name', name)
                config.set('Core', 'Module', file_name)
                config.add_section('Documentation')
                config.set('Documentation', 'Author', author if author != '' else socket.gethostname())
                config.set('Documentation', 'Version', float(version) if version != '' else 1.0)
                config.set('Documentation', 'Website',  website if website != '' else 'http://www.orbotech.com/')
                config.set('Documentation', 'Description', description if description != '' else name)

                config.add_section('Default Values')
                for default in plugins_default_values.items():
                    config.set('Default Values', default[0], default[1])

                config.write(plugin_file)
            self.onUpdatePluginsList()
            self.onChartPluginInfoClear()

    def onTablePluginInfoClear(self):
        """
        Function that  clears the table plug-ins information.

        """
        self.dialog.ledtTablePluginPath.setText('')
        self.dialog.ledtTablePluginName.setText('')
        self.dialog.ledtTableAuthor.setText('')
        self.dialog.ledtTableVersion.setText('')
        self.dialog.ledtTableWebsite.setText('')
        self.dialog.ledtTableDescription.setText('')
# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'C:\Users\YanivP\Desktop\phm\serverUI\UI\PluginParametersDialog.ui'
#
# Created: Sun May 17 15:47:19 2015
#      by: pyside-uic 0.2.15 running on PySide 1.2.2
#
# WARNING! All changes made in this file will be lost!

from PySide import QtCore, QtGui

class Ui_PluginParametersDialog(object):
    def setupUi(self, PluginParametersDialog):
        PluginParametersDialog.setObjectName("PluginParametersDialog")
        PluginParametersDialog.setWindowModality(QtCore.Qt.NonModal)
        PluginParametersDialog.resize(395, 280)
        PluginParametersDialog.setModal(True)
        self.gridLayout = QtGui.QGridLayout(PluginParametersDialog)
        self.gridLayout.setContentsMargins(-1, 11, -1, -1)
        self.gridLayout.setObjectName("gridLayout")
        self.btnClear = QtGui.QPushButton(PluginParametersDialog)
        self.btnClear.setMinimumSize(QtCore.QSize(0, 30))
        self.btnClear.setObjectName("btnClear")
        self.gridLayout.addWidget(self.btnClear, 2, 1, 1, 1)
        self.btnApply = QtGui.QPushButton(PluginParametersDialog)
        self.btnApply.setMinimumSize(QtCore.QSize(0, 30))
        self.btnApply.setObjectName("btnApply")
        self.gridLayout.addWidget(self.btnApply, 2, 0, 1, 1)
        self.btnCancel = QtGui.QPushButton(PluginParametersDialog)
        self.btnCancel.setMinimumSize(QtCore.QSize(0, 30))
        self.btnCancel.setObjectName("btnCancel")
        self.gridLayout.addWidget(self.btnCancel, 2, 2, 1, 1)
        self.splitter = QtGui.QSplitter(PluginParametersDialog)
        self.splitter.setMidLineWidth(7)
        self.splitter.setOrientation(QtCore.Qt.Vertical)
        self.splitter.setHandleWidth(5)
        self.splitter.setObjectName("splitter")
        self.layoutWidget = QtGui.QWidget(self.splitter)
        self.layoutWidget.setObjectName("layoutWidget")
        self.verticalLayout = QtGui.QVBoxLayout(self.layoutWidget)
        self.verticalLayout.setSpacing(5)
        self.verticalLayout.setSizeConstraint(QtGui.QLayout.SetDefaultConstraint)
        self.verticalLayout.setContentsMargins(5, 5, 5, 5)
        self.verticalLayout.setContentsMargins(0, 0, 0, 0)
        self.verticalLayout.setObjectName("verticalLayout")
        self.scrollArea = QtGui.QScrollArea(self.layoutWidget)
        self.scrollArea.setMinimumSize(QtCore.QSize(0, 0))
        self.scrollArea.setWidgetResizable(True)
        self.scrollArea.setObjectName("scrollArea")
        self.scrollAreaLyt = QtGui.QWidget()
        self.scrollAreaLyt.setGeometry(QtCore.QRect(0, 0, 363, 185))
        self.scrollAreaLyt.setObjectName("scrollAreaLyt")
        self.gridLayout_2 = QtGui.QGridLayout(self.scrollAreaLyt)
        self.gridLayout_2.setObjectName("gridLayout_2")
        self.lblParam = QtGui.QLabel(self.scrollAreaLyt)
        self.lblParam.setMinimumSize(QtCore.QSize(0, 0))
        self.lblParam.setMaximumSize(QtCore.QSize(16777215, 16777215))
        font = QtGui.QFont()
        font.setPointSize(10)
        self.lblParam.setFont(font)
        self.lblParam.setObjectName("lblParam")
        self.gridLayout_2.addWidget(self.lblParam, 0, 0, 1, 1)
        self.ledtParam = QtGui.QLineEdit(self.scrollAreaLyt)
        self.ledtParam.setObjectName("ledtParam")
        self.gridLayout_2.addWidget(self.ledtParam, 0, 1, 1, 1)
        self.scrollArea.setWidget(self.scrollAreaLyt)
        self.verticalLayout.addWidget(self.scrollArea)
        self.gridLayout.addWidget(self.splitter, 1, 0, 1, 3)
        self.lblPluginName = QtGui.QLabel(PluginParametersDialog)
        font = QtGui.QFont()
        font.setPointSize(12)
        self.lblPluginName.setFont(font)
        self.lblPluginName.setObjectName("lblPluginName")
        self.gridLayout.addWidget(self.lblPluginName, 0, 0, 1, 3)

        self.retranslateUi(PluginParametersDialog)
        QtCore.QMetaObject.connectSlotsByName(PluginParametersDialog)

    def retranslateUi(self, PluginParametersDialog):
        PluginParametersDialog.setWindowTitle(QtGui.QApplication.translate("PluginParametersDialog", "Plug-in Parameters", None, QtGui.QApplication.UnicodeUTF8))
        self.btnClear.setText(QtGui.QApplication.translate("PluginParametersDialog", "Clear", None, QtGui.QApplication.UnicodeUTF8))
        self.btnApply.setText(QtGui.QApplication.translate("PluginParametersDialog", "Apply", None, QtGui.QApplication.UnicodeUTF8))
        self.btnCancel.setText(QtGui.QApplication.translate("PluginParametersDialog", "Cancel", None, QtGui.QApplication.UnicodeUTF8))
        self.lblParam.setText(QtGui.QApplication.translate("PluginParametersDialog", "Parameter:", None, QtGui.QApplication.UnicodeUTF8))
        self.lblPluginName.setText(QtGui.QApplication.translate("PluginParametersDialog", "Plugin Name - default values", None, QtGui.QApplication.UnicodeUTF8))


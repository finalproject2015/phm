Tutorial Documentation
======================

Introduction
------------

This Software Maintenance Guide designed to help with the software maintenance stage for modification of the PHM software product after delivery to correct faults, to improve performance or other attributes (such as adding features etc.).
Software maintenance is a very broad activity that includes error correction, enhancements of capabilities, deletion of absolete capabilities, and optimization. Because change is inevitable, this Guide designed to help with understanding the code, the architecture, and for easier evaluation, controlling and making modifications.
This Guide purpose is to help preserving the value of software over the time. The value can be enhanced by expanding the PHM system, meeting additional requirements, becoming easier to use, more efficient and employing newer technology


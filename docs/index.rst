.. Prognostic Health Monitor documentation master file, created by
   sphinx-quickstart on Wed Jul 15 09:18:13 2015.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to PHM's documentation!
=====================================================

This is our Introduction to this Project.

Contents:
===============

.. toctree::

	project
	tutorial
	code

Indices and tables
==================

* :ref:`genindex`
* :ref:`search`


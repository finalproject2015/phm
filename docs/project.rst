Project Summary
===============

The Problem
-----------

In the modern world, the mass production industry should be
operational 24/7. Uncontrolled shutdown of the production process
can result in the loss of M$ in revenues. Hardware failures of
components in the production system is one of the major cause
of product line downtime. Prediction of such failures can decrease
those loses.

The Solution
------------

PHM – Prognostic Health Monitor is a system for diagnosing and
analyzing hardware components data. The system assists the
physicists and algorithms developers to explore the components
behavior with easy to use visualization UI. After fast analysis of
massive amounts of data (“big data”) the algorithm can predict
the component’s Remaining Useful Lifetime (RUL) and notify the
operator prior to component failure.

Features
--------

+ Hardware Component Analysis Platform
+ Special Events Occurrences
+ Components Characteristic Correlation

Multiple ways to match user search

+ Natural Language Request
+ Easy Request Editor
+ Personal Favorites

Extendable Plug-ins Mechanism

+ Remaining Useful Lifetime (RUL)
+ Algorithms Exploration Environment
+ On The Fly Development
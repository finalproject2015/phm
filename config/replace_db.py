# -*- coding: utf-8 -*-

import os
from datetime import datetime
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import create_engine, Column, Integer, String, Float, create_engine
from sqlalchemy.orm import sessionmaker
from sqlalchemy.sql import text


Base = declarative_base()
engine = create_engine('sqlite:///D:\\diamond\\DiamondDB\\DiamondDb.db')
session = sessionmaker()
session.configure(bind=engine)

################################################################
# Creating new LED_MEASURES Table                              #
################################################################


class LedMeasure(Base):
    __tablename__ = 'LED_MEASURES'
    ID = Column(Integer, primary_key=True)
    TIME = Column(Integer)
    MACHINE = Column(String)
    HEAD_NUM = Column(Integer)
    LED_NUM = Column(Integer)
    WANTED_CURRENT = Column(Float)
    MEASURED_CURRENT = Column(Float)
    MEASURED_VOLTAGE = Column(Float)
    MEASURED_POWER = Column(Float)
    THERMISTOR_TEMP = Column(Float)

    def __repr__(self):
        return "<LED_MEASURES(ID='%s', TIME='%s', MACHINE='%s', HEAD_NUM='%s', LED_NUM='%s', WANTED_CURRENT='%s'," \
               " MEASURED_CURRENT='%s', MEASURED_VOLTAGE='%s', MEASURED_POWER='%s', THERMISTOR_TEMP='%s')>" % \
               (self.ID, self.TIME, self.MACHINE, self.HEAD_NUM, self.LED_NUM, self.WANTED_CURRENT,
                self.MEASURED_CURRENT, self.MEASURED_VOLTAGE, self.MEASURED_POWER, self.THERMISTOR_TEMP)

Base.metadata.create_all(engine)

################################################################
# Converting the samples from old to new Table                 #
################################################################
s = session()
connection = engine.connect()
sql = text('SELECT MEASURES_SET.*, MEASURED_POWER.POWER'
           ' FROM MEASURES_SET'
           ' INNER JOIN MEASURED_POWER'
           ' ON MEASURES_SET.ID=MEASURED_POWER.MEASURES_SET_ID'
           ' ORDER BY MEASURES_SET.ID')
measures = connection.execute(sql)
id = 1
for row in measures:
    for ledNum in range(1, 9):
        if row['WANTED_CURR_%d' % ledNum] != 0:
            measure = LedMeasure(ID=id,
                                 TIME=row['TIME'],
                                 MACHINE='PT2',
                                 HEAD_NUM=row['HEAD_NUM'],
                                 LED_NUM=ledNum,
                                 WANTED_CURRENT=row['WANTED_CURR_%d' % ledNum],
                                 MEASURED_CURRENT=row['MEASURED_CURR_%d' % ledNum],
                                 MEASURED_VOLTAGE=row['MEASURED_VOLT_%d' % ledNum],
                                 MEASURED_POWER=row['POWER'],
                                 THERMISTOR_TEMP=row['THERMISTOR_TEMP'])
            print 'Adding record: ' + measure.__repr__()
            s.add(measure)
            id += 1

s.commit()
connection.close()
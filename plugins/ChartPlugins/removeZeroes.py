
from yapsy.IPlugin import IPlugin


class removeZerosPlugin(IPlugin):

    def run(self, df_data, events):
        return df_data[df_data != 0]
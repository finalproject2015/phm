
from yapsy.IPlugin import IPlugin
from pandas.stats.moments import rolling_mean


class movingAveragePlugin(IPlugin):

    def run(self, df_data, events, window=5):
        shift = - ((window-1)/2)
        df_data = rolling_mean(df_data, window=window).shift(shift)
        return df_data.dropna()
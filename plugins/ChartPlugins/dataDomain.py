
from yapsy.IPlugin import IPlugin


class dataDomainPlugin(IPlugin):

    def run(self, df_data, events, min_value=22.2, max_value=22.6):
        mask = ((df_data['Temperature'] >= min_value) & (df_data['Temperature'] <= max_value))
        return df_data.loc[mask]
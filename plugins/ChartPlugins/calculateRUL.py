
import time
from yapsy.IPlugin import IPlugin


class calculateRULPlugin(IPlugin):

    def run(self, df_data, events):
        future_time = time.time() * 1000 + 1209600000  # two weeks
        label = "Remaining Useful Lifetime"
        description = "The element is at risk stage\n and must be replaced immediately"
        events.add_event(future_time, label, description)
        df_data.loc[future_time] = None
        df_data.Temperature.loc[future_time] = 27
        df_data.Voltage.loc[future_time] = 2.5
        df_data.Current.loc[future_time] = 1.5

        return df_data
# -*- coding: utf-8 -*-

import pandas as pd
from yapsy.IPlugin import IPlugin
from phm.domain.ConfigurationFileReader import ConfigurationFileReader
from phm.domain.utils import rangeGenerator


class degradationTablePlugin(IPlugin):

    def run(self, df_table, referencePoints=3):
        entities = ConfigurationFileReader().getMeasureEntities().keys()

        last_entity = ConfigurationFileReader().getMeasureEntityElements(entities[-1])
        short = [rangeGenerator(sc['#text']) for sc in last_entity if sc['@name'] == 'short'][0]
        long = [rangeGenerator(sc['#text']) for sc in last_entity if sc['@name'] == 'long'][0]

        new_df = pd.DataFrame(columns=entities+['Degradation'])
        for x in set([tuple(x) for x in df_table[entities].values]):

            new_row = {}
            curr_df = df_table
            for i, entity in enumerate(entities):
                curr_df = curr_df[curr_df[entity] == x[i]]
                new_row[entity] = x[i]

            sampleUnit = curr_df[:referencePoints]['Current'].values
            avgSamples = sum(sampleUnit) / len(sampleUnit)
            lastSample = curr_df[-1:]['Current'].values[0]

            new_row['Degradation'] = '%f%%' % (((lastSample - avgSamples)*100)/avgSamples)
            new_df.loc[len(new_df)] = new_row

        #  all/short/long
        measured_entity = entities[-1]
        for x in set([tuple(x) for x in df_table[entities[:-1]].values]):

            new_row = {}
            curr_df = new_df
            for i, entity in enumerate(entities[:-1]):
                curr_df = curr_df[curr_df[entity] == x[i]]
                new_row[entity] = x[i]

            measures = {led: float(deg.strip('%')) for led, deg in curr_df[[measured_entity, 'Degradation']].values}

            #  all
            avgSamples = sum(measures.values()) / len(measures.values())
            new_row[measured_entity] = 'all'
            new_row['Degradation'] = '%f%%' % avgSamples
            new_df.loc[len(new_df)] = new_row

            # short
            short_measures = [v for k, v in measures.iteritems() if k in short]
            if len(short_measures):
                avgSamples = sum(short_measures) / len(short_measures)
                new_row[measured_entity] = 'short'
                new_row['Degradation'] = '%f%%' % avgSamples
                new_df.loc[len(new_df)] = new_row

            # long
            long_measures = [v for k, v in measures.iteritems() if k in long]
            if len(long_measures):
                avgSamples = sum(long_measures) / len(long_measures)
                new_row[measured_entity] = 'long'
                new_row['Degradation'] = '%f%%' % avgSamples
                new_df.loc[len(new_df)] = new_row

        return new_df.sort(entities, ascending=True)
